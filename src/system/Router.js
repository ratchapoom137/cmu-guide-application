import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-native'
import { Provider } from 'react-redux'
import { store, history } from '../system/AppStore';
import { ConnectedRouter } from 'connected-react-router'
import LoginPage from '../pages/LoginPage'
import RegisterPage from '../pages/RegisterPage'
import Footer from '../components/general/Footer'
import HomePage from '../pages/HomePage'
import MapPage from '../pages/MapPage'
import ShopListPage from '../pages/ShopListPage'
import ShopPage from '../pages/ShopPage'
import SimilarShop from '../pages/SimilarShop'
import SplashScreen from '../pages/LoaderLanguage'
// import Detail from '../pages/Tabs/DetailTab'
import WriteReviewPage from '../pages/WriteReviewPage'
import ReviewHistoryPage from '../pages/ReviewHistoryPage'
import LoaderScreen from '../pages/LoaderScreen'
import TripPage from '../pages/TripPage'
import AddTripPage from '../pages/AddTripPage'
import TripDetailPage from '../pages/TripDetailPage'
import {
    ROUTE_HOME,
    ROUTE_LOGIN,
    ROUTE_REGISTER,
    ROUTE_MAP,
    ROUTE_FOOTER,
    ROUTE_SHOPLIST,
    ROUTE_SHOP,
    ROUTE_SIMILARSHOP,
    ROUTE_SPLASH,
    ROUTE_WRITEREVIEW,
    ROUTE_REVIEWHISTORY,
    ROUTE_LOADER,
    ROUTE_TRIP,
    ROUTE_ADDTRIP,
    ROUTE_TRIPDETAIL
} from '../constants/RouteConstants'

class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route path={ROUTE_TRIP} component={TripPage} />
                        <Route path={ROUTE_LOGIN} component={LoginPage} />
                        <Route path={ROUTE_REGISTER} component={RegisterPage} />
                        <Route path={ROUTE_FOOTER} component={Footer} />
                        <Route path={ROUTE_HOME} component={HomePage} />
                        <Route path={ROUTE_MAP} component={MapPage} />
                        <Route path={ROUTE_SHOPLIST} component={ShopListPage} />
                        <Route path={ROUTE_SHOP} component={ShopPage} />
                        <Route path={ROUTE_SIMILARSHOP} component={SimilarShop} />
                        <Route path={ROUTE_SPLASH} component={SplashScreen} />
                        <Route path={ROUTE_WRITEREVIEW} component={WriteReviewPage} />
                        <Route path={ROUTE_REVIEWHISTORY} component={ReviewHistoryPage} />
                        <Route path={ROUTE_LOADER} component={LoaderScreen} />
                        <Route path={ROUTE_ADDTRIP} component={AddTripPage} />
                        <Route path={ROUTE_TRIPDETAIL} component={TripDetailPage} />
                        <Redirect to={ROUTE_FOOTER} />
                    </Switch>
                </ConnectedRouter>
            </Provider>
        )
    }
}

export default Router