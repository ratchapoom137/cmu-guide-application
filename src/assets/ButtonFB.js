import React, { Component } from 'react';
import { StyleSheet, View, Alert } from 'react-native';
import { Button, Icon } from '@ant-design/react-native'
import { LoginManager, AccessToken, GraphRequestManager, GraphRequest } from 'react-native-fbsdk'
import { connect } from 'react-redux'

class ButtonFB extends Component {

    state = {
        email: '',
        firstName: '',
        lastName: '',
        imageUrl: ''
    }

    _fbAuth() {
        let { addUser, history } = this.props;
        let { navigateToRegisterPage } = this
        LoginManager.logInWithReadPermissions(["public_profile", 'email']).then(
            function (result) {
                if (result.isCancelled) {
                    console.log("Login cancelled");
                } else {
                    console.log(
                        "Login success with permissions: " +
                        result.grantedPermissions.toString()
                    );
                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            let accessToken = data.accessToken;
                            const responseInfoCallback = (error, result) => {
                                setTimeout(() => {
                                    if (error) {
                                        Alert.alert('Error' + error.toString());
                                    } else {
                                        if (result.email == undefined) {
                                            Alert.alert('Error', "Email address is required.");
                                        } else {
                                            Alert.alert('Login Success', 'Email: ' + result.email + ' Name: ' + result.name + 'Image: ' + result.picture.data.url);
                                            let user = { email: result.email, firstName: result.first_name, lastName: result.last_name, imageUrl: result.picture.data.url}
                                            addUser(user);
                                        }
                                    }
                                }, 200);
                            }
                            const infoRequest = new GraphRequest(
                                '/me',
                                {
                                    accessToken: accessToken,
                                    parameters: {
                                        fields: {
                                            string: 'email,name,first_name,middle_name,last_name,picture'
                                        }
                                    }
                                },
                                responseInfoCallback
                            );
                            new GraphRequestManager().addRequest(infoRequest).start();
                        }
                    )
                }
            },
            function (error) {
                console.log("Login fail with error: " + error);
            }
        );
    }

    render() {
        console.log("POOM: "+this.props.user)
        return (
            <View style={styles.container}>
                <Button style={styles.facebookButton} onPress={this._fbAuth.bind(this)} type="primary"><Icon name="facebook" size="sm" color="white" /> Login with Facebook</Button>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    facebookButton: {
        width: 300,
        marginVertical: 10,
        paddingHorizontal: 12,
        height: 38
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDidpatchToProps = (dispatch) => {
    return {
        addUser: (user) => {
            dispatch({
                type: 'ADD_USER',
                user: user
            })
        }
    }
}

export default connect(
    mapStateToProps,
    mapDidpatchToProps
)(ButtonFB)