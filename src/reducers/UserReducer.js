export default (state = {}, action) => {
    switch (action.type) {
        case 'ADD_USER':
            return {
                ...state, 
                information : action.user
            }
        case 'EDIT_USER':
            return {
                ...state,
                firstName: action.firstName,
                lastName: action.lastName,
            }
        case 'SET_LANGUAGE':
            return {
                ...state,
                language: action.language
            }
        case 'LOGOUT':
            return {
                
            }
        default:
            return state
    }
}