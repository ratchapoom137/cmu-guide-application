export const setLanguage = (language) => (
    {
        type: 'SET_LANGUAGE',
        language: language
    }
)

export const addUser = (user) => (
    {
        type: 'ADD_USER',
        user: user
    }
)

export const logOut = () => (
    {
        type: 'LOGOUT',
    }
)