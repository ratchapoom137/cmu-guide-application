import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Alert, ImageBackground } from 'react-native';
import { Icon, InputItem, Button } from '@ant-design/react-native';
import { SocialIcon } from 'react-native-elements'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import { setLanguage, addUser } from '../reducers/action'
import Logo from '../assets/Logo'
import Loader from '../components/items/Loader'
import axios from 'axios';
import { LoginManager, AccessToken, GraphRequestManager, GraphRequest } from 'react-native-fbsdk'
import {
    Body,
    SignupTextCont,
    SignupText,
    SignupButton,
    LoginButton
} from '../components/General.styled'
import {
    ROUTE_HOME,
    ROUTE_REGISTER,
    ROUTE_FOOTER,
} from '../constants/RouteConstants'

class LoginPage extends Component {

    state = {
        email: '',
        password: '',
        firstName: '',
        lastName: '',
        imageUrl: '',
        loading: false,
        lang: ''
    }

    componentWillMount() {
        const { user, setLanguage } = this.props
        if (!user.language) {
            setLanguage('th')
        }
    }

    navigateToRegisterPage = () => {
        this.props.push(ROUTE_REGISTER)
    }

    onClickLogin = () => {
        const { addUser, push, setLanguage, user } = this.props
        if (this.state.email == '' || this.state.password == '') {
            Alert.alert('Please do not leave input fields blanked')
        } else {
            this.setState({ loading: true })
            axios({
                url: 'http://35.173.241.14:8888/auth',
                method: 'post',
                data: {
                    username: this.state.email,
                    password: this.state.password,
                    // username: 'B',
                    // password: '1'
                }
            }).then(res => {
                const { data } = res
                addUser(data);
                setTimeout(() => {
                    this.setState({ loading: false })
                    push(ROUTE_FOOTER)
                }, 1500)
            }).catch(e => {
                this.setState({ loading: false })
                alert('Email or passwordsss ' + e.response.data.errors)
                console.log("asdoqowejqwoewqo", e.response);
                
            })
        }
    }

    onRegister = (user) => {
        console.log("regisssssssssssssssssssss", user);
        axios({
            url: 'http://35.173.241.14:8888/user/register',
            method: 'post',
            data: {
                email: user.email,
                password: '1',
                firstName: user.firstName,
                lastName: user.lastName,
                image: user.imageUrl
            }
        }).then(res => {
            this.setState({
                email: user.email,
                password: '1'
            })
            this.onClickLogin()
        }).catch(e => {
            if (e.response.status === 422) {
                this.setState({
                    email: user.email,
                    password: '1'
                })
                this.onClickLogin()
            } else {
                alert('Email or password ' + e.response.data.errors)
                console.log("eeeeeee", e.response);
                
            }
        })
    }

    _fbAuth() {
        let { addUser, push } = this.props;
        let { onClickLogin, onRegister } = this
        this.setState({ visibleLoginFacebook: true })
        LoginManager.logInWithReadPermissions(["public_profile", 'email']).then(
            function (result) {
                if (result.isCancelled) {
                    console.log("Login cancelled");
                } else {
                    console.log(
                        "Login success with permissions: " +
                        result.grantedPermissions.toString()
                    );
                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            let accessToken = data.accessToken;
                            const responseInfoCallback = (error, result) => {
                                setTimeout(() => {
                                    if (error) {
                                        Alert.alert('Error' + error.toString());
                                    } else {
                                        if (result.email == undefined) {
                                            Alert.alert('Error', "Email address is required.");
                                        } else {
                                            Alert.alert('Login Success', 'Email: ' + result.email + ' Name: ' + result.name);
                                            let user = { email: result.email, firstName: result.first_name, lastName: result.last_name, imageUrl: result.picture.data.url, accessToken: accessToken }
                                            // addUser(user);
                                            onRegister(user)
                                        }
                                    }
                                }, 200);
                            }
                            const infoRequest = new GraphRequest(
                                '/me',
                                {
                                    accessToken: accessToken,
                                    parameters: {
                                        fields: {
                                            string: 'email,name,first_name,middle_name,last_name,picture'
                                        }
                                    }
                                },
                                responseInfoCallback
                            );
                            new GraphRequestManager().addRequest(infoRequest).start();
                        }
                    )
                }
            },
            function (error) {
                console.log("Login fail with error: " + error);
            }
        );
    }

    onPressSkip = () => {
        this.props.push(ROUTE_FOOTER)
    }

    render() {
        const { user } = this.props
        if (user.language === 'th') {
            titleFacebook = 'เข้าสู่ระบบด้วย Facebook'
        } else {
            titleFacebook = 'Sign in with Facebook'
        }
        return (
            <Body>
                <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                    <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                        <TouchableOpacity style={{ alignContent: 'center', justifyContent: 'center', marginLeft: 14, marginTop: 10 }} onPress={() => this.onPressSkip()}>
                            <Text style={{ color: '#d9d9d9', fontSize: 18 }}>{user.language == 'th' ? "กลับ" : user.language == "en" ? "Back" : ''}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end' }}>
                        <TouchableOpacity style={{ alignContent: 'center', justifyContent: 'center', marginRight: 14, marginTop: 10 }} onPress={() => this.onPressSkip()}>
                            <Text style={{ color: '#d9d9d9', fontSize: 18 }}>{user.language == 'th' ? "ข้าม" : user.language == "en" ? "Skip" : ''}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <Logo />
                <View style={{ width: '90%' }}>
                    <InputItem
                        style={{ color: '#ffffff' }}
                        clear
                        value={this.state.email}
                        onChange={email => {
                            this.setState({
                                email,
                            });
                        }}
                        placeholder={user.language == 'th' ? 'อีเมลล์' : user.language == 'en' ? 'Email' : ''}
                    >
                        <Icon name="user" size="md" color="white" />
                    </InputItem>
                </View>
                <View style={{ width: '90%' }}>
                    <InputItem
                        style={{ color: '#ffffff' }}
                        clear
                        type="password"
                        secureTextEntry={true}
                        value={this.state.password}
                        onChange={password => {
                            this.setState({
                                password,
                            });
                        }}
                        placeholder={user.language == 'th' ? 'รหัสผ่าน' : user.language == 'en' ? 'Password' : ''}
                    >
                        <Icon name="lock" size="md" color="white" />
                    </InputItem>
                </View>
                <LoginButton>
                    <Button
                        onPress={() => this.onClickLogin()}
                        activeStyle={{ backgroundColor: '#d9d9d9' }}
                        style={styles.loginButton}
                        type="primary"

                    >
                        <Text style={{ color: 'white' }}>{user.language == 'th' ? 'เข้าสู่ระบบ' : user.language == 'en' ? 'Login' : ''}</Text>
                    </Button>
                </LoginButton>
                <Text style={{ color: 'gray' }}>───────────────────────────</Text>
                {/* <Button style={styles.facebookButton} onPress={() => this._fbAuth()} type="primary"><Icon name="facebook" size="sm" color="white" /> Login with Facebook</Button> */}
                <SocialIcon
                    style={{ width: '70%' }}
                    title={titleFacebook}
                    button
                    type='facebook'
                    onPress={() => this._fbAuth()}
                />
                <SignupTextCont>
                    <SignupText>{user.language == 'th' ? 'ยังไม่มีบัญชีใช่ไหม?' : user.language == 'en' ? 'Dont have an account yet?' : ''}</SignupText>
                    <TouchableOpacity onPress={() => { this.navigateToRegisterPage() }}><SignupButton> {user.language == 'th' ? 'ลงทะเบียน' : user.language == 'en' ? 'Signup' : ''}</SignupButton></TouchableOpacity>
                </SignupTextCont>
                <Loader
                    modalVisible={this.state.loading}
                    animationType="fade"
                />
            </Body>
        );
    }

}

const styles = StyleSheet.create({
    loginButton: {
        backgroundColor: '#ff9900',
        borderRadius: 30,
        borderColor: '#ff9900'
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    { setLanguage, addUser, push }
)(LoginPage)
