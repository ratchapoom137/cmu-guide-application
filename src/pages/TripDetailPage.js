import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, FlatList, Alert } from 'react-native';
import { SwipeAction, Button, Card, Grid, InputItem } from '@ant-design/react-native';
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import IconMat from 'react-native-vector-icons/MaterialCommunityIcons';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconFont5 from 'react-native-vector-icons/FontAwesome5';
import IconFont from 'react-native-vector-icons/FontAwesome';
import IconFeather from 'react-native-vector-icons/Feather';
import axios from 'axios';
import SubHeader from '../components/general/SubHeader'
import Loader from '../components/items/Loader'
import _ from 'lodash';
const data = Array.from(new Array(9)).map((_val, i) => ({
    icon: 'https://os.alipayobjects.com/rmsportal/IptWdCkrtkAUfjE.png',
    text: `Name${i}`,
}));
import {
    Content,
    BodyMain,
    Body,
    Center,
    ShopNameText,
    ShopItemContent
} from '../components/General.styled'
import {
    ROUTE_SHOPLIST,
    ROUTE_SHOP,
    ROUTE_ADDTRIP,
    ROUTE_LOADER
} from '../constants/RouteConstants'

class TripDetailPage extends Component {

    state = {
        modal: false,
        tripOrderList: [],
        shop: [],
        loading: true,
        check: false,
        buttonOption: [
            {
                text: 'Edit',
                onPress: () => this.onPressEditButton(),
                style: { backgroundColor: 'orange', color: 'white' },
            },
            {
                text: 'Delete',
                onPress: () => this.onPressDeleteButton(),
                style: { backgroundColor: 'red', color: 'white' },
            },
        ],
        itemOnFocus: []
    }

    componentDidMount() {
        this.renderTrip()
        // this.renderShop()
    }

    renderTrip = () => {
        const { user } = this.props
        console.log("state iddddd", this.props.location.state.id);
        axios({
            url: `http://35.173.241.14:8888/trip/listby/${this.props.location.state.id}`,
            method: 'get',
            headers: { "Authorization": `Bearer ${user.information.token}` },
        }).then(res => {
            this.setState({
                tripOrderList: res.data
            })
            if (res.data.length === 0) {
                this.setState({
                    check: true,
                    loading: false
                })
            }
            res.data.map((item, index) => {
                this.renderShop(item.shopId)
            })
            // console.log("Chceck tripOrderList ", res.data.length);

        }).catch(e => {
            this.setState({
                loading: false
            })
            alert('not successed' + e.response.data)
            console.log('res data: ', e.response.data);
            console.log('res data: ', e.response);
        })
    }

    renderShop = (shopId) => {
        // console.log("swqeqewewq", this.state.tripOrderList);

        axios.get(`https://chiangmai.thaimarket.guide/shop/${shopId}`)
            .then(response => {
                this.setState({
                    shop: [...this.state.shop, response.data.data],
                    loading: false
                })
                this.mapShopAndTripOrder()
            })
            .catch((error) => {
                console.error('Error: ' + error);
            })

    }

    onPressAddTrip = () => {
        const { push } = this.props
        push(ROUTE_SHOPLIST, { category: 'รวม', tripId: this.props.location.state.id })
    }

    _keyExtractor = (item, index) => item.id;

    navigationToShopPage = (id) => {
        const { push } = this.props
        push(ROUTE_SHOP, { id, tripId: this.props.location.state.id })
    }

    onPressDeleteButton = () => {
        const { user } = this.props
        Alert.alert(
            'ต้องการที่จะลบรีวิว ?',
            '',
            [{
                text: 'ลบรีวิว', onPress: () => {
                    axios({
                        url: `http://35.173.241.14:8888/trip/triporder/delete/${this.state.itemOnFocus.item[0].id}`,
                        method: 'delete',
                        headers: { "Authorization": `Bearer ${user.information.token}` }
                    }).then(res => {
                        alert('Delete successed')
                        this.props.push(ROUTE_LOADER)
                        // this.renderTrip()
                    }).catch(e => {
                        alert('Delete not successed' + e.response.data)
                        console.log('res data: ', e.response.data);
                        console.log('res data: ', e.response);
                    })
                }
            }]
        );
    }

    mapShopAndTripOrder = () => {
        const test = _.map(this.state.shop, (s) => {
            const item = _.filter(this.state.tripOrderList, { 'shopId': s.id})
            return {
                ...s, item
            }
        })

        this.setState({
            shop: test
        })
    }

    render() {
        const { user } = this.props
        // const footerButtons = [
        //     { text: 'Cancel', onPress: () => this.setState({ modal: false }) },
        //     { text: 'Ok', onPress: () => this.onPressAddTrip() },
        // ];
        console.log("Shoppppwww,", this.state.itemOnFocus);
        if (this.state.shop.length > 0 && this.state || this.state.check === true) {
            return (
                <BodyMain>
                    <SubHeader title={user.language == 'th' ? 'รายละเอียดทริป' : user.language == 'en' ? 'Trip Detail' : ''} hideGoBack={'back'} />
                    <ScrollView>
                        <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                            <Button
                                style={styles.shadow}
                                size="small"
                                onPress={() => this.onPressAddTrip()}
                            >
                                {user.language == 'th' ? 'เพิ่มสถานที่' : user.language == 'en' ? 'Add Location' : ''}
                            </Button>
                        </View>
                        <View style={{ marginLeft: 20, marginBottom: 10 }}>
                            <Text style={{ fontSize: 20 }}>{user.language == 'th' ? 'จำนวน: ' : user.language == 'en' ? 'Number: ' : ''}{this.state.tripOrderList.length}</Text>
                        </View>


                        <FlatList
                            style={{ backgroundColor: 'blue' }}
                            numColumns={1}
                            data={this.state.shop}
                            keyExtractor={this._keyExtractor}
                            renderItem={(item, index) => (
                                <SwipeAction
                                    autoClose
                                    style={{ backgroundColor: 'transparent' }}
                                    right={this.state.buttonOption}
                                    onOpen={() => this.setState({ itemOnFocus: item.item })}
                                    onClose={() => this.setState({ itemOnFocus: item.item })}
                                >
                                    <Card full>
                                        <Card.Header
                                            title={user.language == 'th' ? <View style={{ width: 250 }}><ShopNameText numberOfLines={1}>{item.item.lang.th.name}</ShopNameText></View> : user.language == 'en' ? <ShopNameText>{item.item.lang.en.name}</ShopNameText> : ''}
                                            thumb={
                                                item.item.category == 'ร้านค้า' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/shop.png')} /> :
                                                    item.item.category == 'ร้านอาหาร (และเครื่องดื่ม)' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/food-drink.png')} /> :
                                                        item.item.category == 'เครื่องประดับ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/accessories.png')} /> :
                                                            item.item.category == 'คาเฟ่และของหวาน' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/cafe.png')} /> :
                                                                item.item.category == 'บริการต่างๆ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/service.png')} /> :
                                                                    item.item.category == 'อาหารไทย' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/thaiFood.png')} /> :
                                                                        item.item.category == 'อาหารเอเชีย' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/noodles.png')} /> :
                                                                            item.item.category == 'สินค้าเบ็ดเตล็ด และอื่นๆ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/more-item.png')} /> :
                                                                                item.item.category == 'เฟอร์นิเจอร์และของตกแต่งบ้าน' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/forniture.png')} /> :
                                                                                    item.item.category == 'ของที่ระลึก' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/gift.png')} /> :
                                                                                        item.item.category == 'ศูนย์การค้า' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/mall.png')} /> :
                                                                                            item.item.category == 'ต้นไม้และอุปกรณ์ทำสวน' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/tree-icon.png')} /> :
                                                                                                item.item.category == 'ของเก่า ของสะสม' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/collections.png')} /> :
                                                                                                    item.item.category == 'สุขภาพและความงาม' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/heart.png')} /> :
                                                                                                        item.item.category == 'ตลาด' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/market-icon.png')} /> :
                                                                                                            item.item.category == 'แฟชั่นสตรี' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/fashion-women.png')} /> :
                                                                                                                item.item.category == 'แฟชั่นบุรุษ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/fashion-men.png')} /> : <Icon style={{ right: 8 }} name="file-unknown" size={20} />
                                            }
                                            extra={
                                                <View style={{ alignItems: 'flex-end' }}>
                                                    <TouchableOpacity style={{ paddingHorizontal: 10 }}>
                                                        <IconMat name="crosshairs-gps" size={24} />
                                                    </TouchableOpacity>
                                                </View>}
                                        />
                                        <TouchableOpacity onPress={() => { this.navigationToShopPage(item.item.id) }}>
                                            <Card.Body>
                                                <ShopItemContent>
                                                    <Image style={{ width: 140, height: 160, resizeMode: 'contain' }} source={{ uri: item.item.image }} />
                                                    <View style={styles.width}>
                                                        <ShopItemContent>
                                                            <View style={styles.border}>
                                                                <TouchableOpacity style={styles.menuButton} >
                                                                    <IconFont style={{ marginStart: 5 }} name="star" size={18} color='#ffb31a' />
                                                                    <View style={{ margin: 5 }}>
                                                                        <Text>
                                                                            {/* {item.item.status != "ACTIVE" ? item.item.status : '0.0'} */}
                                                                            {item.item.averageRate}
                                                                        </Text>
                                                                    </View>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.border}>
                                                                <TouchableOpacity style={styles.menuButton} >
                                                                    <IconFont5 style={{ marginStart: 5 }} name="map-marker-alt" size={18} color='#999999' />
                                                                    <View style={{ margin: 5 }}><Text>5 km.</Text></View>
                                                                </TouchableOpacity>
                                                            </View>
                                                            {item.item.highlighted == true ? <IconMat style={{ margin: 4 }} name="trophy-award" size={20} color="#9900cc" /> : console.log()}
                                                        </ShopItemContent>
                                                        <View style={{ width: '60%' }}>
                                                            <Text numberOfLines={5}>
                                                                {user.language == 'th' ? item.item.lang.th.description : user.language == 'en' ? item.item.lang.en.description : ''}
                                                            </Text>
                                                        </View>
                                                    </View>
                                                </ShopItemContent>
                                            </Card.Body>
                                            {/* <Card.Footer content="footer content" extra="footer extra content" /> */}
                                        </TouchableOpacity>
                                    </Card>
                                </SwipeAction>
                            )}
                        />

                    </ScrollView>
                    <Loader
                        modalVisible={this.state.loading}
                        animationType="fade"
                    />
                </BodyMain>
            )
        } else {
            return (
                <BodyMain>

                </BodyMain>
            )
        }
    }
}

const styles = StyleSheet.create({
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 6,
        borderRadius: 40,
        width: 100,
        height: 40,
        margin: 15
    },
    boxImageProduct: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 6,
        height: '70%',
        backgroundColor: 'white'
    },
    imageProducts: {
        width: 85,
        height: 120,
        resizeMode: 'contain',
        marginRight: 5,
        marginLeft: 5
    },
    menuButton: {
        borderRadius: 8,
        margin: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    border: {
        backgroundColor: '#cccccc',
        borderRadius: 8,
        margin: 2
    },
    width: {
        width: '100%'
    },
    height: {
        height: '100%'
    }
})

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    { push }
)(TripDetailPage)