import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Toast, InputItem, Button } from '@ant-design/react-native';
import { connect } from 'react-redux'
import { push, goBack } from 'connected-react-router'
import axios from 'axios';
import {
    ROUTE_SHOP
} from '../constants/RouteConstants'
import SubHeader from '../components/general/SubHeader'
import StarRating from 'react-native-star-rating';
import {
    Content,
    BodyMain,
    Body,
    Center,
    ShopNameText,
    ShopItemContent
} from '../components/General.styled'
class AddTripPage extends Component {

    state = {
        name: '',
        description: ''
    }

    onPressAddTrip = () => {
        const { user, goBack } = this.props
        axios({
            url: 'http://35.173.241.14:8888/trip/add',
            method: 'post',
            headers: { "Authorization": `Bearer ${user.information.token}` },
            data: {
                name: this.state.name,
                description: this.state.description,
                customer: {
                    id: user.information.user.id
                }
            }
        }).then(res => {
            alert('add successed')
            goBack()
        }).catch(e => {
            alert('not successed' + e.response.data)
            this.setState({ modal: false })
        })
    }

    render() {
        const { user, data } = this.props
        return (
            <BodyMain>
                <SubHeader title={user.language == 'th' ? 'เพิ่มทริป' : user.language == 'en' ? 'Add Trip' : ''} hideGoBack={'back'} />
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <InputItem
                        clear
                        value={this.state.name}
                        onChange={value => {
                            this.setState({
                                name: value
                            });
                        }}
                        placeholder="กรุณาใส่ชื่อทริป"
                    >
                    </InputItem>
                    <InputItem
                        clear
                        value={this.state.description}
                        onChange={value => {
                            this.setState({
                                description: value
                            });
                        }}
                        placeholder="กรุณาใส่รายละเอียด"
                    >
                    </InputItem>
                    <Button
                        style={styles.shadow}
                        size="small"
                        onPress={() => this.onPressAddTrip()}
                    >
                        {user.language == 'th' ? 'เพิ่มทริป' : user.language == 'en' ? 'Add Trip' : ''}
                    </Button>
                </View>
            </BodyMain>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        alignItems: 'center'
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 6,
        borderRadius: 40,
        width: 100,
        height: 40,
        margin: 15
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    { push, goBack }
)(AddTripPage)
