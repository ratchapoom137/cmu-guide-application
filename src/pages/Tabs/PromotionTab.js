import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Dimensions, StatusBar, ScrollView } from 'react-native';
import { Icon, Tabs } from '@ant-design/react-native';

export default class PromotionTab extends Component {

    state = {
        promotion: []
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', alignItems: 'center' }}>
                <Image
                    style={{ width: 250, height: 250, resizeMode: 'contain' }}
                    source={require('../../assets/image/promo-icon.png')}
                />
                <Text style={{ fontSize: 20, color: '#8c8c8c' }}>No Promotions</Text>
            </View>
        )
    }
}