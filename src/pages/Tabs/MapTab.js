import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
} from 'react-native';
import { Icon, Button, Card } from '@ant-design/react-native'
import IconMat from 'react-native-vector-icons/MaterialCommunityIcons';
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import shopIcon from '../../assets/image/shop-icon-map2.png'
import currentIcon from '../../assets/image/currentLocation-icon.png'
import MapView, { Marker } from 'react-native-maps';

class MapTab extends Component {

    state = {
        latitude: parseFloat(this.props.data.location.latitude),
        longitude: parseFloat(this.props.data.location.longitude),
        shop: []
    }

    renderCurrentLocation = () => {
        navigator.geolocation.getCurrentPosition(position => {
            this.setState({
                // visibleCurrentlocation: true,
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                error: null,
            });
        });
    }

    render() {
        const { user, data } = this.props
        return (
            <View style={styles.container}>
                <MapView
                    style={{ position: 'relative' }}
                    style={styles.map}
                    showsUserLocation={true}
                    region={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121
                    }}
                >
                    {/* {
                        this.state.latitudeCurrentLocation != 0 && this.state.longitudeCurrentLocation != 0 ?
                            <Marker
                                coordinate={{
                                    latitude: this.state.latitudeCurrentLocation,
                                    longitude: this.state.longitudeCurrentLocation,
                                }}
                                title="Your Location"
                                image={currentIcon}
                            />
                            : console.log('null')
                    } */}
                    <Marker
                        coordinate={{
                            latitude: parseFloat(data.location.latitude),
                            longitude: parseFloat(data.location.longitude),
                        }}
                        title={user.language == 'th' ? data.lang.th.name : user.language == 'en' ? data.lang.en.name : ''}
                        description={"Status: " + data.status}
                        image={shopIcon}
                    />
                </MapView>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.infoButton}>
                        <Icon name="info-circle" size={24} color="gray" />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.currentlocationButton} onPress={() => this.renderCurrentLocation()}>
                        <IconMat name="crosshairs-gps" size={24} color="gray" />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '70%',
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    buttonContainer: {
        flexDirection: 'column',
        marginVertical: 20,
        backgroundColor: 'transparent',
    },
    currentlocationButton: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        padding: 15,
        marginTop: 15,
        marginRight: 15,
        borderRadius: 50,
        elevation: 6,
        backgroundColor: 'white'
    },
    infoButton: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        padding: 15,
        marginRight: 15,
        marginBottom: 5,
        borderRadius: 50,
        elevation: 6,
        backgroundColor: 'white'
    },
    menuButton: {
        borderRadius: 8,
        margin: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    border: {
        backgroundColor: '#cccccc',
        borderRadius: 8,
        margin: 2
    },
    width: {
        width: '100%'
    },
    height: {
        height: '100%'
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    { push }
)(MapTab)