import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, Alert } from 'react-native';
import { Icon, Tabs, Grid, Modal, Button, Provider, Toast } from '@ant-design/react-native';
import { SocialIcon } from 'react-native-elements';
import { TabNavigatorConfig } from 'react-navigation';
import { push, goBack } from 'connected-react-router'
import { connect } from 'react-redux'
import axios from 'axios';
import {
    Row,
    BoxBackgroudColor,
    TextCustomer,
    Flex,
    Center
} from '../../components/General.styled'
import {
    ROUTE_SIMILARSHOP,
    ROUTE_SHOP
} from '../../constants/RouteConstants'

class DetailTab extends Component {

    state = {
        shopList: [],
        similarShop: [],
        visibleProduct: false,
        visibleDescriptionShop: false,
        imageModal: '',
        descriptionModal: '',
        nameModal: '',
        descriptionShopModal: ''
    }

    componentWillMount() {
        this.renderShop()
        console.log("pathNameeeee:", this.props.pathName);
        
    }

    renderShop = () => {
        const { data } = this.props
        axios.get('https://chiangmai.thaimarket.guide/shop?offset=0&limit=0&fbclid=IwAR32UgvD_cRbbC9dQHocuUAcrQ_Ic-pnZ1EGxqGeU_FXnDkStmzcwX1F3OY')
            .then(response => {
                response.data.data.map((item) => {
                    if (item.category == data.category && item.id != data.id) {
                        this.state.similarShop.push(item)
                    }
                })
                this.setState({
                    shopList: response.data
                })
                // console.log('ALL SHOP' + this.state.shopList.data[0].image)
            })
            .catch((error) => {
                console.error('Error: ' + error);
            })
    }

    onClose = () => {
        this.setState({ visibleProduct: false, visibleDescriptionShop: false })
    }

    onClickProduct = (item) => {
        const { user } = this.props
        if (user.language == 'th') {
            this.setState({ nameModal: item.lang.th.name, descriptionModal: item.lang.th.description })
        } else if (user.language == 'en') {
            this.setState({ nameModal: item.lang.en.name, descriptionModal: item.lang.en.description })
        }
        this.setState({ visibleProduct: true, imageModal: item.image })
    }

    onClickMore = () => {
        this.setState({ visibleDescriptionShop: true })
    }

    navigationToSimilarShop = (id) => {
        const { push, pathName } = this.props
        if(pathName == '/shop') {
            push(ROUTE_SIMILARSHOP, { id })
        } else {
            push(ROUTE_SHOP, { id })
        }
        
    }

    render() {
        const { user, data } = this.props

        console.log('product2 : ' + this.state.shopList)
        console.log('in render' + this.state.similarShop)
        if (this.state.shopList.data && this.state.similarShop) {
            return (
                <View>
                    <ScrollView style={styles.boxProduct} horizontal={true}>
                        <Row products>
                            <Grid
                                hasLine={false}
                                itemStyle={styles.gridItemStyle}
                                data={data.products}
                                columnNum={data.products.length}
                                renderItem={(item, index) => (
                                    <TouchableOpacity
                                        style={styles.boxImageProduct}
                                        onPress={() => this.onClickProduct(item)}
                                    >
                                        <Image source={{ uri: item.image }}
                                            style={styles.imageProducts}
                                        />
                                        <Center style={styles.nameProduct}>
                                            <Text numberOfLines={1}>{user.language == 'th' ? item.lang.th.name : user.language == 'en' ? item.lang.en.name : ''}</Text>
                                        </Center>
                                    </TouchableOpacity>
                                )}
                            />
                        </Row>
                    </ScrollView>
                    <BoxBackgroudColor white style={styles.detailBox}>
                        <Icon name="tag" color="#7d5194" />
                        <TextCustomer numberOfLines={5}>
                            {user.language == 'th' ? data.lang.th.description : user.language == 'en' ? data.lang.en.description : ''}
                        </TextCustomer>
                        <View style={styles.boxMoreButton}>
                            <TouchableOpacity style={styles.MoreButton} onPress={() => this.onClickMore()}>
                                <TextCustomer moreButton>{user.language == 'th' ? 'อ่านเพิ่มเติม' : user.language == 'en' ? 'Read More' : ''}</TextCustomer>
                            </TouchableOpacity>
                        </View>
                    </BoxBackgroudColor>
                    <BoxBackgroudColor white style={styles.boxContact}>
                        <View style={styles.iconContact}>
                            <SocialIcon
                                style={{ height: 34, width: 34 }} type='facebook' iconSize={20}
                            />
                            <SocialIcon
                                style={{ height: 34, width: 34 }} type='instagram' iconSize={20}
                            />
                            <SocialIcon
                                style={{ height: 34, width: 34 }} type='twitter' iconSize={20}
                            />
                            <SocialIcon
                                style={{ height: 34, width: 34 }} type='youtube' iconSize={20}
                            />
                        </View>
                    </BoxBackgroudColor>
                    <View style={styles.boxTextSimilarStore}>
                        <Text>{user.language == 'th' ? 'ร้านค้าที่คล้ายกัน' : user.language == 'en' ? 'Similar Stores' : ''}</Text>
                    </View>
                    <ScrollView style={styles.boxSimilarStore} horizontal={true}>
                        <Row products>
                            <Grid
                                hasLine={false}
                                itemStyle={styles.gridItemSimilarShopStyle}
                                data={this.state.similarShop}
                                columnNum={this.state.similarShop.length}
                                renderItem={(item) => (
                                    <TouchableOpacity
                                        style={styles.center}
                                        onPress={() => this.navigationToSimilarShop(item.id)}
                                    >
                                        <Image source={{ uri: item.image }}
                                            style={styles.imageSimilarShop}
                                        />
                                        <Text numberOfLines={1}>{user.language == 'th' ? item.lang.th.name : user.language == 'en' ? item.lang.en.name : ''}</Text>
                                    </TouchableOpacity>
                                )}
                            />
                        </Row>
                    </ScrollView>

                    {/* Modal */}

                    <Provider>
                        <Modal
                            style={{ backgroundColor: 'rgba(0,0,0,0.6)', alignItems: 'center', justifyContent: 'center', height: '100%', width: '100%' }}
                            transparent={true}
                            visible={this.state.visibleProduct}
                            animationType="slide-up"
                            onClose={this.onClose}
                        >
                            <View style={styles.bodyModal}>
                                <View style={styles.boxCloseButton}>
                                    <TouchableOpacity onPress={this.onClose}>
                                        <Icon name="close" size={28} color="#7d5194" />
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.center}>
                                    <Image source={{ uri: this.state.imageModal }}
                                        style={styles.imageProductsModal}
                                    />
                                    <Center style={{ margin: 5 }}>
                                        <TextCustomer titleModal>
                                            {this.state.nameModal}
                                        </TextCustomer>
                                    </Center>
                                    <Center style={{ marginLeft: 4, marginRight: 4, marginBottom: 30 }}>
                                        <TextCustomer desModal>
                                            {this.state.descriptionModal}
                                        </TextCustomer>
                                    </Center>
                                </View>
                            </View>
                        </Modal>

                        <Modal
                            style={{ backgroundColor: 'rgba(0,0,0,0.6)', alignItems: 'center', justifyContent: 'center', height: '100%', width: '100%' }}
                            transparent={true}
                            visible={this.state.visibleDescriptionShop}
                            animationType="slide-up"
                            onClose={this.onClose}
                        >
                            <View style={styles.bodyModal}>
                                <View style={styles.boxCloseButton}>
                                    <TouchableOpacity onPress={this.onClose}>
                                        <Icon name="close" size={28} color="#7d5194" />
                                    </TouchableOpacity>
                                </View>
                                <View style={[styles.center, { width: '80%', marginBottom: 30 }]}>
                                    <TextCustomer titleModal>
                                        {user.language == 'th' ? data.lang.th.description : user.language == 'en' ? data.lang.en.description : ''}
                                    </TextCustomer>
                                </View>
                            </View>
                        </Modal>
                    </Provider>
                </View >
            );
        } else {
            return (
                <View>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    boxProduct: {
        backgroundColor: '#f2f2f2',
        height: 250,
    },
    boxSimilarStore: {
        backgroundColor: '#f2f2f2',
        height: 200,
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageProducts: {
        width: 140,
        height: '90%',
        resizeMode: 'contain',
        marginRight: 5,
        marginLeft: 5
    },
    imageProductsModal: {
        width: 300,
        height: 300,
        resizeMode: 'contain',
        margin: 5
    },
    imageSimilarShop: {
        width: 146,
        height: '86%',
        resizeMode: 'contain',
        marginRight: 5,
        marginLeft: 5
    },
    gridItemStyle: {
        margin: 5,
        height: '90%',
        width: 150,
        backgroundColor: 'rgba(255,255,255,0.8)'
    },
    gridItemSimilarShopStyle: {
        margin: 5,
        height: '94%',
        width: 140,
        backgroundColor: 'rgba(255,255,255,0.8)',
    },
    detailBox: {
        height: 200,
        padding: 25
    },
    MoreButton: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    boxMoreButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    boxContact: {
        height: 50,
        marginTop: 10,
        paddingLeft: 25,
        paddingRight: 25
    },
    iconContact: {
        alignItems: 'center',
        flexDirection: 'row',
        height: '100%'
    },
    nameProduct: {
        width: 140
    },
    boxTextSimilarStore: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        margin: 10
    },
    boxImageProduct: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 6,
        height: '100%',
        backgroundColor: 'white'
    },
    bodyModal: {
        backgroundColor: 'rgba(0,0,0,0.6)',
        margin: 10,
        borderRadius: 10,
        width: 340,
        alignItems: 'center',
        justifyContent: 'center'
    },
    bodyModal: {
        backgroundColor: 'rgba(0,0,0,0.8)',
        margin: 10,
        borderRadius: 10,
        width: 340,
        alignItems: 'center',
        justifyContent: 'center'
    },
    boxCloseButton: {
        width: '100%',
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginTop: 8,
        marginBottom: 8
    }
})

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    { push, goBack }
)(DetailTab)