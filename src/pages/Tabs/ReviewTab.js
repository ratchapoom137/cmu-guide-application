import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Dimensions, Alert, TouchableOpacity } from 'react-native';
import { Card, Button, Modal, Provider, Icon } from '@ant-design/react-native';
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import axios from 'axios';
import Loader from '../../components/items/Loader'
import IconFont from 'react-native-vector-icons/FontAwesome';
import IconFont5 from 'react-native-vector-icons/FontAwesome5';
import {
    Content,
    BodyMain,
    Body,
    Menu,
    ShopNameText,
    ShopItemContent
} from '../../components/General.styled'
import {
    ROUTE_LOGIN,
    ROUTE_WRITEREVIEW,
    ROUTE_SIMILARSHOP
} from '../../constants/RouteConstants'
var options = { weekday: 'short', month: 'short', day: 'numeric' }

class ReviewTab extends Component {

    state = {
        review: [],
        loading: false,
        customerInReview: [],
        visible: false,
        buttonOption: [
            {
                text: 'Edit',
                onPress: () => console.log('Edit'),
                style: { backgroundColor: 'orange', color: 'white' },
            },
            {
                text: 'Delete',
                onPress: () => console.log('Delete'),
                style: { backgroundColor: 'red', color: 'white' },
            },
        ]
    }

    componentDidMount() {
        this.renderAllReview()
        // this.getUserByCustomerId()
    }

    renderAllReview = () => {
        const { data } = this.props
        this.setState({ loading: true })
        axios.get(`http://35.173.241.14:8888/review/shop/${data.id}`)
            .then(response => {
                console.log("REs: ", response.data);
                
                response.data.map((item) => {
                    if (item.deleted == false) {
                        this.state.review.push(item)
                    }
                })
                this.setState({
                    // review: response.data,
                    loading: false
                })
                this.state.review.map((item, index) => {
                    //    console.log("user ja: "+this.getUserByCustomerId(item.customerId));
                    this.getUserByCustomerId(item.customerId, index)
                    console.log("customerInReview: ", this.state.customerInReview);

                })
                console.log("USER in review: ", this.state.review);

            })
            .catch((error) => {
                console.error('Error: ' + error);
            })
    }

    // componentDidUpdate() {
    //     if (this.state.review.length > 0) {
    //         axios.get(`http://35.173.241.14:8888/user/${customerId}`)
    //             .then(response => {
    //                 // this.state.customerInReview.push(response.data)
    //                 this.state
    //             })
    //             .catch((error) => {
    //                 console.error('Error: ' + error);
    //             })
    //     }
    // }

    renderStar = (item) => {
        let star = [
            { color: '#999999', iconName: 'star-o' },
            { color: '#999999', iconName: 'star-o' },
            { color: '#999999', iconName: 'star-o' },
            { color: '#999999', iconName: 'star-o' },
            { color: '#999999', iconName: 'star-o' }
        ]

        for (let i = 0; i < Math.round(item); i++) {
            star[i].color = '#ffb31a';
            star[i].iconName = 'star';
        }
        return (
            <View style={styles.boxStar}>
                <IconFont name={star[0].iconName} size={16} color={star[0].color} />
                <IconFont name={star[1].iconName} size={16} color={star[1].color} />
                <IconFont name={star[2].iconName} size={16} color={star[2].color} />
                <IconFont name={star[3].iconName} size={16} color={star[3].color} />
                <IconFont name={star[4].iconName} size={16} color={star[4].color} />
            </View>
        )
    }

    renderDate = (timestamp) => {
        var date = new Date(timestamp).toLocaleDateString("th-TH", options)
        var time = new Date(timestamp).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
        return (
            <Text>{date} at {time}</Text>
        )
    }

    getUserByCustomerId = (customerId, index) => {
        axios.get(`http://35.173.241.14:8888/user/${customerId}`)
            .then(response => {
                // this.state.review[index].customerId = response.data
                // [...this.state.customerInReview, response.data]
                this.setState({
                    customerInReview: [...this.state.customerInReview, response.data]
                })
                console.log("customerInReview222: ", this.state.customerInReview);
            })
            .catch((error) => {
                console.error('Error: ' + error);
            })
    }

    onPressWriteReview = () => {
        const { push, user, data } = this.props
        if (user.information) {
            push(ROUTE_WRITEREVIEW, { data })
        } else {
            push(ROUTE_LOGIN)
        }
        // push(ROUTE_WRITEREVIEW, { data })
    }

    onPressEditButton = (item) => {
        console.log("item send", item);
        
        Alert.alert(
            'ต้องการที่จะแก้ไขรีวิว ?',
            '',
            [{ text: 'ไปที่หน้าแก้ไขรีวิว', onPress: () => { this.props.push(ROUTE_WRITEREVIEW, { item }) } }]
        );
    }

    onPressDeleteButton = (item) => {
        const { data, user } = this.props
        Alert.alert(
            'ต้องการที่จะลบ ?',
            '',
            [{
                text: 'ลบ', onPress: () => {
                    axios({
                        url: `http://35.173.241.14:8888/review/delete/${item.id}`,
                        method: 'delete',
                        headers: { "Authorization": `Bearer ${user.information.token}` }
                    }).then(res => {
                        alert('Delete successed')
                        setTimeout(() => {
                            this.props.push(ROUTE_SIMILARSHOP, { deleted: 'deleted', id: data.id })
                        }, 1500)
                    }).catch(e => {
                        alert('Delete not successed' + e.response.data)
                        console.log('res data: ', e.response.data);
                        console.log('res data: ', e.response);
                    })
                }
            }]
        );
    }

    render() {
        const { user } = this.props
        if (this.state.review.length > 0) {
            console.log("Review finish:", this.state.review[0].customerId);
            if (this.state.customerInReview.length > 0) {
                console.log("Review finish111:", this.state.customerInReview[0].firstName);
                console.log("Review finish111:", this.state.customerInReview[1]);
                return (
                    <View style={{ flex: 1 }}>
                        <View style={{ flex: 1 }}>
                            <View style={{ width: '100%', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                                <Button
                                    style={styles.shadow}
                                    size="small"
                                    onPress={() => { this.onPressWriteReview() }}
                                >
                                    {user.language == 'th' ? 'เขียนรีวิว' : user.language == 'en' ? 'Write a review' : ''}
                                </Button>
                            </View>
                            <View style={{ flex: 1 }}>
                                {
                                    this.state.review.map((item, index) => {
                                        return (
                                            <Card full>
                                                <Card.Body>
                                                    <View style={{ flexDirection: 'row', margin: 10, marginLeft: 15 }}>
                                                        {
                                                            this.state.customerInReview[index] != undefined ? (
                                                                this.state.customerInReview[index].image == null ? (
                                                                    <Image
                                                                        style={styles.imageProfileUser}
                                                                        source={require('../../assets/image/imageUser2.png')}
                                                                    />
                                                                ) : (
                                                                        <Image
                                                                            style={styles.imageProfileUser}
                                                                            source={{ uri: this.state.customerInReview[index].image }}
                                                                        />
                                                                    )
                                                            ) : null
                                                        }
                                                        <View style={{ flex: 1 }}>
                                                            {
                                                                this.state.customerInReview[index] != undefined ?
                                                                    <Text style={{ fontSize: 18, color: 'black', fontWeight: '500' }}>{this.state.customerInReview[index].firstName}</Text>
                                                                    : null

                                                            }
                                                            <View style={{ flexDirection: 'row' }}>
                                                                <Text style={{ color: '#ffb31a' }}>{item.rate}.0</Text>
                                                                {this.renderStar(item.rate)}
                                                            </View>
                                                            {this.renderDate(item.date)}
                                                        </View>
                                                        <View>
                                                            {
                                                                this.state.customerInReview[index] != undefined && user.information != null ?
                                                                    (
                                                                        this.state.customerInReview[index].id == user.information.user.id ?
                                                                            (
                                                                                <View>
                                                                                    <TouchableOpacity style={{ width: 24, margin: 6 }} onPress={() => this.onPressEditButton(item)}>
                                                                                        <Icon name="edit" size={20} color="#e67300" />
                                                                                    </TouchableOpacity>
                                                                                    <TouchableOpacity style={{ width: 24, margin: 6 }} onPress={() => this.onPressDeleteButton(item)}>
                                                                                        <Icon name="delete" size={20} color="#e60000" />
                                                                                    </TouchableOpacity>
                                                                                </View>
                                                                            ) : (
                                                                                <View>
                                                                                    <TouchableOpacity style={{ width: 24, margin: 6 }} onPress={() => { }}>
                                                                                        <IconFont5 name="ellipsis-v" size={20} />
                                                                                    </TouchableOpacity>
                                                                                </View>
                                                                            )
                                                                    ) : null
                                                            }
                                                        </View>
                                                    </View>
                                                    <View style={{ marginLeft: 25, width: '90%' }}>
                                                        <Text>{item.comment}</Text>
                                                    </View>
                                                    {/* {
                                                        this.state.customerInReview[index] != undefined ? 
                                                        (
                                                            this.state.customerInReview[index].id == user.information.user.id ?
                                                            (
                                                                <View style={{ marginLeft: 25, width: '90%' }}>
                                                                    <Text numberOfLines={1}>{item.comment}</Text>
                                                                </View>
                                                            ) : (
                                                                null
                                                            )
                                                        ) : null
                                                    } */}
                                                </Card.Body>
                                                {/* <Card.Footer content="footer content" extra="footer extra content" /> */}
                                            </Card>
                                        )
                                    })
                                }
                            </View>
                        </View>
                        {/* <Provider>
                            <Modal
                                title={user.language == 'th' ? 'ตัวเลือก' : user.language == 'en' ? 'Option' : ''}
                                transparent={true}
                                onClose={() => this.onClose()}
                                maskClosable
                                visible={this.state.visible}
                                closable
                            >
                                <Button size="small" style={{ height: 30 }} onPress={() => this.onPressEdit()}>
                                    {user.language == 'th' ? 'แก้ไข' : user.language == 'en' ? 'Edit' : ''}
                                </Button>
                                <Button size="small" style={{ height: 30 }} onPress={() => this.onPressDelete()} >
                                    {user.language == 'th' ? 'ลบ' : user.language == 'en' ? 'Delete' : ''}
                                </Button>
                            </Modal>
                        </Provider> */}
                    </View>
                )
            } else {
                return (
                    <View>

                    </View>
                )
            }
        } else {
            return (
                <View>
                    <View style={{ flex: 1 }}>
                        <View style={{ flex: 1 }}>
                            <View style={{ width: '100%', height: 70, alignItems: 'center', justifyContent: 'center' }}>
                                <Button
                                    style={styles.shadow}
                                    size="small"
                                    onPress={() => { this.onPressWriteReview() }}
                                >
                                    {user.language == 'th' ? 'เขียนรีวิว' : user.language == 'en' ? 'Write a review' : ''}
                                </Button>
                            </View>
                        </View>
                    </View>
                    <Loader
                        modalVisible={this.state.loading}
                        animationType="fade"
                    />
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 6,
        borderRadius: 40,
        width: 100,
        height: 40
    },
    imageProfileUser: {
        width: 70,
        height: 70,
        resizeMode: 'cover',
        borderRadius: 70 / 2,
        marginRight: 15
    },
    boxStar: {
        marginLeft: 5,
        flexDirection: 'row'
    },
})

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    { push }
)(ReviewTab)