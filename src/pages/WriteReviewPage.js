import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Toast, InputItem, Button } from '@ant-design/react-native';
import { connect } from 'react-redux'
import { push, goBack } from 'connected-react-router'
import axios from 'axios';
import {
    ROUTE_SHOP
} from '../constants/RouteConstants'
import SubHeader from '../components/general/SubHeader'
import StarRating from 'react-native-star-rating';

class WriteReviewPage extends Component {

    state = {
        starCount: (this.props.location.state.item == undefined ? 0 : this.props.location.state.item.rate),
        review: (this.props.location.state.item == undefined ? '' : this.props.location.state.item.comment)
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }

    onPressEditPost = () => {
        const { goBack, user } = this.props
        axios({
            url: 'http://35.173.241.14:8888/review/edit',
            method: 'put',
            headers: { "Authorization": `Bearer ${user.information.token}` },
            data: {
                comment: this.state.review,
                rate: this.state.starCount,
                id: this.props.location.state.item.id,
                date: Date.now()
            }
        }).then(res => {
            alert('successed')
            setTimeout(() => {
                goBack()
            }, 1500)
        }).catch(e => {
            alert('not successed' + e.response.data)
            console.log('res data: ', e.response.data);
            console.log('res data: ', e.response);
        })

    }

    onPressPost = () => {
        const { goBack, user } = this.props
        axios({
            url: 'http://35.173.241.14:8888/review/add',
            method: 'post',
            headers: { "Authorization": `Bearer ${user.information.token}` },
            data: {
                comment: this.state.review,
                rate: this.state.starCount,
                shopId: this.props.location.state.data.id,
                customerId: user.information.user.id,
                date: Date.now()
            }
        }).then(res => {
            alert('successed')
            setTimeout(() => {
                goBack()
            }, 1500)
        }).catch(e => {
            alert('not successed' + e.response.data)
            console.log('res data: ', e.response.data);
            console.log('res data: ', e.response);
        })
    }

    render() {
        const { user, data } = this.props
        // console.log('data id: ', this.props.location.state.data.id);
        // console.log('user token: ', user.information.token);
        // console.log('user id: ', user.information.user.id);
        // console.log('star: ', this.state.starCount);
        // console.log('review: ', this.state.review);
        // console.log('date: ', Date.now());
        return (
            <View style={styles.container}>
                <SubHeader title={user.language == 'th' ? 'เขียนรีวิว' : user.language == 'en' ? 'Write a review' : ''} hideGoBack={'back'} />
                <View style={{ height: 140, width: '100%', backgroundColor: 'rgba(0,0,0,0.7)', alignItems: 'center', justifyContent: 'center' }}>
                    <StarRating
                        disabled={false}
                        maxStars={5}
                        rating={this.state.starCount}
                        selectedStar={(rating) => this.onStarRatingPress(rating)}
                        fullStarColor={'#ffb31a'}
                    />
                    <View style={{ marginTop: 15 }}>
                        <Text style={{ color: '#e6e6e6' }}>{user.language == 'th' ? 'ให้คะแนนร้าน' : user.language == 'en' ? 'Tap on star to give rating' : ''}</Text>
                    </View>
                </View>
                <View style={{ width: '100%', marginLeft: 10, marginBottom: 15 }}>
                    <Text>{user.language == 'th' ? 'เขียนรีวิวร้านค้าที่นี่...' : user.language == 'en' ? 'Write review here...' : ''}</Text>
                    <InputItem
                        clear
                        value={this.state.review}
                        onChange={value => {
                            this.setState({
                                review: value
                            });
                        }}
                    />
                </View>
                {
                    this.props.location.state.item == undefined ?
                        (
                            <Button
                                size="small"
                                style={{ height: 40, width: 100, backgroundColor: '#ff9900' }}
                                onPress={() => this.onPressPost()}
                            >
                                <Text style={{ fontSize: 16 }}>{user.language == 'th' ? 'โพส' : user.language == 'en' ? 'Post' : ''}</Text>
                            </Button>
                        ) : (
                            <Button
                                size="small"
                                style={{ height: 40, width: 100, backgroundColor: '#ff9900' }}
                                onPress={() => this.onPressEditPost()}
                            >
                                <Text style={{ fontSize: 16 }}>{user.language == 'th' ? 'แก้ไข' : user.language == 'en' ? 'Edit' : ''}</Text>
                            </Button>
                        )
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    { push, goBack }
)(WriteReviewPage)
