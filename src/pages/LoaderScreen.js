import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity, ImageBackground, Animated, Button } from 'react-native';
import { TabBar, Icon, Carousel } from '@ant-design/react-native'
import { goBack } from 'connected-react-router'
import { connect } from 'react-redux'
import Loader from '../components/items/Loader'
import { push } from 'connected-react-router'
import Header from '../components/general/Header'
import {
    BodyMain,
    Content,
    ImageCarousel,
    TextCarousel,
    BoxTextCarousel,
    CarouselBox,
    Menu,
    ItemMenu,
    MenuStyle,
} from '../components/General.styled'
import {
    ROUTE_FOOTER,
    ROUTE_REVIEWHISTORY
} from '../constants/RouteConstants'

class LoaderScreen extends Component {

    state = {
        loading: false
    }

    componentDidMount() {
        const { goBack } = this.props
        goBack()
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                {/* <Loader
                    modalVisible={this.state.loading}
                    animationType="fade"
                /> */}
            </View>
        )
    }
}

export default connect(
    null,
    { goBack }
)(LoaderScreen)