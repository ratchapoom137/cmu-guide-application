import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, Picker } from 'react-native';
import { Toast, Icon, List, Modal, Provider, Button } from '@ant-design/react-native'
import Header from '../components/general/Header'
import IconFont5 from 'react-native-vector-icons/FontAwesome5';
import Loader from '../components/items/Loader'
import { push } from 'connected-react-router'
import { logOut } from '../reducers/action'
import { setLanguage } from '../reducers/action'
import { connect } from 'react-redux'
import {
    Content,
    BodyMain
} from '../components/General.styled'
import {
    ROUTE_LOGIN,
    ROUTE_FOOTER,
    ROUTE_SPLASH
} from '../constants/RouteConstants'
const Item = List.Item;

class MorePage extends Component {

    state = {
        language: 'th',
        visible: false,
        visibleLogOut: false
    }

    onClickLanguage = () => {
        this.setState({ visible: true })
        // Toast.info('sadssad')
        // this.props.setLanguage(item)
    }

    onClickLanguageItem = (item) => {
        this.setState({ visible: false })
        if (item == 'th') {
            Toast.info('ไทย')
        } else if (item == 'en') {
            Toast.info('English')
        }
        this.props.setLanguage(item)
        this.props.push(ROUTE_SPLASH)
    }

    onClose = () => {
        this.setState({ visible: false })
    }

    onPressLogOut = () => {
        const { push, logOut } = this.props
        this.setState({visibleLogOut: true})
        this.props.logOut()
        setTimeout(() => {
            this.setState({visibleLogOut: false})
            push(ROUTE_LOGIN)
        }, 1500)
    }

    render() {
        const { user } = this.props
        const footerButtons = [
            { text: 'Cancel', onPress: () => console.log('cancel') },
            { text: 'Ok', onPress: () => console.log('ok') },
        ];
        return (
            <BodyMain>
                <Header title='MORE ' />
                <View style={{ width: 360 }}>
                    {/* <Picker
                        style={{ width: '80%' }}
                        selectedValue={this.state.language}
                        onValueChange={(item) => Toast.info('sadssad')}
                    >
                        <Picker.Item label="Thai" value="th" />
                        <Picker.Item label="English" value="en" />
                    </Picker> */}
                    <List renderHeader={'Setting'}>
                        <Item arrow="down" onPress={() => this.onClickLanguage()} >
                            <IconFont5 name="language" size={24} color="purple" />
                            <Text style={{ color: 'black', fontSize: 20 }}>{user.language == 'th' ? 'ภาษา' : user.language == 'en' ? 'Language' : ''}</Text>
                        </Item>
                    </List>
                    {
                        user.information != null ?
                        <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', margin: 10 }}>
                            <Button style={{ width: '50%', backgroundColor: '#ff9900', borderRadius: 50 }} onPress={() => this.onPressLogOut()}>
                                <Text style={{ color: 'white'}}>{user.language == 'th' ? 'ออกจากระบบ' : user.language == 'en' ? 'Logout' : ''}</Text>
                            </Button>
                        </View>
                        : null
                    }
                </View>
                <Provider>
                    <Modal
                        title={user.language == 'th' ? 'ภาษา' : user.language == 'en' ? 'Language' : ''}
                        transparent={true}
                        onClose={() => this.onClose()}
                        maskClosable
                        visible={this.state.visible}
                        closable
                    >
                        <Button size="small" style={{ height: 30 }} onPress={() => this.onClickLanguageItem('th')}>
                            ไทย
                        </Button>
                        <Button size="small" style={{ height: 30 }} onPress={() => this.onClickLanguageItem('en')} >
                            English
                        </Button>
                    </Modal>
                </Provider>
                <Loader
                    modalVisible={this.state.visibleLogOut}
                    animationType="fade"
                />
            </BodyMain>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    { setLanguage, push, logOut }
)(MorePage)