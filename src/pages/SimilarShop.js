import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Dimensions, StatusBar, ScrollView } from 'react-native';
import { Icon, Tabs } from '@ant-design/react-native'
import IconMat from 'react-native-vector-icons/MaterialCommunityIcons';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconFont5 from 'react-native-vector-icons/FontAwesome5';
import IconFont from 'react-native-vector-icons/FontAwesome';
import IconFeather from 'react-native-vector-icons/Feather';
import { push, goBack } from 'connected-react-router'
import { connect } from 'react-redux'
import * as Animatable from 'react-native-animatable';
import { Header } from 'react-navigation';
import MenuTab from '../components/general/TabOnShopPage'
import Loader from '../components/items/Loader'
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';
import axios from 'axios';
import {
    Content,
    BodyMain,
    Body,
    Menu,
    ShopNameText,
    ShopItemContent,
    TextTitleInBoxShop,
    BoxText,
    Flex,
    TouchableOpacityBackButton,
    Center,
    TouchGps1,
    TouchGps2,
    Row
} from '../components/General.styled'
const MIN_HEIGHT = Header.HEIGHT;
const MAX_HEIGHT = 550;

class SimilarShopPage extends Component {

    state = {
        shop: [],
        showNavTitle: false,
        rate: 0
    }

    componentDidMount() {
        this.renderShopById()
        this.renderReviewListByShopId()
    }

    renderShopById = () => {
        axios.get(`https://chiangmai.thaimarket.guide/shop/${this.props.location.state.id}`)
            .then(response => {
                this.setState({
                    shop: response.data
                })
            })
            .catch((error) => {
                console.error('Error: ' + error);
            })
    }

    renderReviewListByShopId = (shopId, indexShop) => {
        axios.get(`http://35.173.241.14:8888/review/shop/${this.props.location.state.id}`)
            .then(response => {
                let sum = 0
                console.log("resppppp", response.data);
                response.data.map((item, index, arr) => {
                    if (response.data.length > 0 && item.deleted === false) {
                        sum = sum + item.rate
                        if (arr.length - 1 === index) {
                            sum = (sum / arr.length).toFixed(1)
                            this.setState({
                                rate: sum
                            })
                        }
                    }
                })
            })
            .catch((error) => {
                console.error('Error: ' + error);
            })
    }

    renderStar() {
        let star = [
            { color: '#999999', iconName: 'star-o' },
            { color: '#999999', iconName: 'star-o' },
            { color: '#999999', iconName: 'star-o' },
            { color: '#999999', iconName: 'star-o' },
            { color: '#999999', iconName: 'star-o' }
        ]

        for (let i = 0; i < Math.round(this.state.rate); i++) {
            star[i].color = '#ffb31a';
            star[i].iconName = 'star';
        }
        return (
            <View style={styles.boxStar}>
                <IconFont name={star[0].iconName} size={16} color={star[0].color} />
                <IconFont name={star[1].iconName} size={16} color={star[1].color} />
                <IconFont name={star[2].iconName} size={16} color={star[2].color} />
                <IconFont name={star[3].iconName} size={16} color={star[3].color} />
                <IconFont name={star[4].iconName} size={16} color={star[4].color} />
            </View>
        )
    }

    render() {
        const { user, title, hideGoBack, goBack } = this.props
        const tabs = [
            { title: 'First Tab' },
            { title: 'Second Tab' },
            { title: 'Third Tab' }
        ];
        const style = {
            alignItems: 'center',
            justifyContent: 'center',
            height: 150,
            backgroundColor: '#fff',
        };
        console.log("Rate jaaa: ", this.state.rate);

        if (this.state.shop.data) {
            return (
                <Flex style={{ backgroundColor: 'white' }}>
                    <StatusBar barStyle="light-content" />
                    <HeaderImageScrollView
                        maxHeight={MAX_HEIGHT}
                        minHeight={MIN_HEIGHT}
                        fadeOutForeground
                        renderHeader={() => <Image source={{ uri: this.state.shop.data.image }} style={styles.image} />}
                        renderFixedForeground={() => (
                            <Animatable.View
                                style={styles.navTitleView}
                                ref={navTitleView => {
                                    this.navTitleView = navTitleView;
                                }}
                            >
                                <TouchableOpacityBackButton style={[styles.navTitle, styles.back, { marginLeft: 15 }]} onPress={goBack}>
                                    <Icon name="left" color="#ffcc99" size={30} />
                                </TouchableOpacityBackButton>
                                <Text style={styles.navTitle}>
                                    {user.language == 'th' ? this.state.shop.data.lang.th.name : user.language == 'en' ? this.state.shop.data.lang.en.name : ''}
                                </Text>
                                <TouchGps2 style={[styles.back, { alignItems: 'flex-end', marginRight: 15 }]}>
                                    <IconMat name="crosshairs-gps" size={24} color="gray" />
                                </TouchGps2>
                            </Animatable.View>
                        )}
                        renderForeground={() => (
                            <Center style={styles.titleContainer}>
                                <Flex backButton>
                                    <TouchableOpacityBackButton full onPress={goBack}>
                                        <Icon name="left" color="#ffcc99" size={45} />
                                    </TouchableOpacityBackButton>
                                </Flex>
                                <View style={styles.styleBoxTitle}>
                                    <BoxText>
                                        <Row>
                                            <TextTitleInBoxShop>{user.language == 'th' ? this.state.shop.data.lang.th.name : user.language == 'en' ? this.state.shop.data.lang.en.name : ''}</TextTitleInBoxShop>
                                            {this.state.shop.data.highlighted == true ? <IconMat style={{ margin: 4 }} name="trophy-award" size={26} color="#9900cc" /> : console.log()}
                                        </Row>
                                        <TouchGps1>
                                            <IconMat name="crosshairs-gps" size={24} color="gray" />
                                        </TouchGps1>
                                    </BoxText>
                                    <BoxText deta>
                                        <TextTitleInBoxShop detail>
                                            {
                                                this.state.shop.data.category == 'ร้านค้า' ? <IconEntypo style={{ right: 8 }} name="shop" size={20} /> :
                                                    this.state.shop.data.category == 'ร้านอาหาร (และเครื่องดื่ม)' ? <IconMat style={{ right: 8 }} name="food" size={20} /> :
                                                        this.state.shop.data.category == 'เครื่องประดับ' ? <IconFont5 style={{ right: 8 }} name="ring" size={20} /> :
                                                            this.state.shop.data.category == 'คาเฟ่และของหวาน' ? <IconFont5 style={{ right: 8 }} name="candy-cane" size={20} /> :
                                                                this.state.shop.data.category == 'บริการต่างๆ' ? <IconMat style={{ right: 8 }} name="room-service" size={20} /> :
                                                                    this.state.shop.data.category == 'อาหารไทย' ? <IconMat style={{ right: 8 }} name="food-variant" size={20} /> :
                                                                        this.state.shop.data.category == 'อาหารเอเชีย' ? <IconMat style={{ right: 8 }} name="food" size={20} /> :
                                                                            this.state.shop.data.category == 'สินค้าเบ็ดเตล็ด และอื่นๆ' ? <IconEntypo style={{ right: 8 }} name="box" size={20} /> :
                                                                                this.state.shop.data.category == 'เฟอร์นิเจอร์และของตกแต่งบ้าน' ? <IconEntypo style={{ right: 8 }} name="image" size={20} /> :
                                                                                    this.state.shop.data.category == 'ของที่ระลึก' ? <IconFeather style={{ right: 8 }} name="gift" size={20} /> :
                                                                                        this.state.shop.data.category == 'ศูนย์การค้า' ? <Icon style={{ right: 8 }} name="shopping-cart" size={20} color="#666666" /> :
                                                                                            this.state.shop.data.category == 'ต้นไม้และอุปกรณ์ทำสวน' ? <IconEntypo style={{ right: 8 }} name="tree" size={20} /> :
                                                                                                this.state.shop.data.category == 'ของเก่า ของสะสม' ? <IconMat style={{ right: 8 }} name="collections" size={20} /> :
                                                                                                    this.state.shop.data.category == 'สุขภาพและความงาม' ? <IconFont style={{ right: 8 }} name="heartbeat" size={20} /> :
                                                                                                        this.state.shop.data.category == 'ตลาด' ? <IconEntypo style={{ right: 8 }} name="shop" size={20} /> :
                                                                                                            this.state.shop.data.category == 'แฟชั่นสตรี' ? <IconMat style={{ right: 8 }} name="tshirt-v-outline" size={20} /> :
                                                                                                                this.state.shop.data.category == 'แฟชั่นบุรุษ' ? <IconMat style={{ right: 8 }} name="tshirt-crew-outline" size={20} /> : <Icon style={{ right: 8 }} name="file-unknown" size={20} />
                                            }
                                            {this.state.shop.data.category}
                                        </TextTitleInBoxShop>
                                    </BoxText>
                                    <ShopItemContent>
                                        <TextTitleInBoxShop review>{this.state.rate}</TextTitleInBoxShop>
                                        {this.renderStar()}
                                    </ShopItemContent>
                                    <TextTitleInBoxShop status>{user.language == 'th' ? 'สถานะ' : user.language == 'en' ? 'Status' : ''} : {this.state.shop.data.status}</TextTitleInBoxShop>
                                    {/* <Center style={{ width: '100%' }}>
                                        <Icon name="up" color="#ffe6cc" />
                                        <Text style={{ color: "#ffe6cc", fontSize: 12 }}>{user.language == 'th' ? 'ดูเพิ่มเติม' : user.language == 'en' ? 'See More' : ''}</Text>
                                    </Center> */}
                                </View>
                            </Center>
                        )}
                    >
                        <TriggeringView
                            style={[styles.section]}
                            onHide={() => this.navTitleView.fadeInUp(200)}
                            onDisplay={() => this.navTitleView.fadeOut(100)}
                        >
                        </TriggeringView>
                        <View style={[styles.section, styles.sectionLarge]}>
                            <MenuTab data={this.state.shop.data} />
                        </View>
                    </ HeaderImageScrollView >
                </Flex >
            );
        } else {
            return (
                <BodyMain>
                    <Loader
                        modalVisible={this.state.loading}
                        animationType="fade"
                    />
                </BodyMain>
            )
        }
    }
}

const styles = StyleSheet.create({
    image: {
        height: MAX_HEIGHT,
        width: Dimensions.get('window').width,
        alignSelf: 'stretch',
        resizeMode: 'cover',
    },
    title: {
        fontSize: 20,
    },
    name: {
        fontWeight: 'bold',
    },
    section: {
        borderBottomWidth: 1,
        borderBottomColor: '#cccccc',
        backgroundColor: '#f2f2f2',
    },
    sectionTitle: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    sectionContent: {
        fontSize: 16,
        textAlign: 'justify',
    },
    keywords: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
    },
    keywordContainer: {
        backgroundColor: '#999999',
        borderRadius: 10,
        margin: 10,
        padding: 10,
    },
    keyword: {
        fontSize: 16,
        color: 'white',
    },
    titleContainer: {
        flex: 1,
        alignSelf: 'stretch',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    imageTitle: {
        color: 'white',
        backgroundColor: 'transparent',
        fontSize: 24,
    },
    navTitleView: {
        height: MIN_HEIGHT,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 16,
        opacity: 0,
        flexDirection: 'row'
    },
    navTitle: {
        color: 'white',
        fontSize: 18,
        backgroundColor: 'transparent',
    },
    sectionLarge: {
        height: 790
        //560
    },
    sectionLarge2: {
        height: 700
    },
    styleBoxTitle: {
        backgroundColor: 'rgba(0,0,0,0.6)',
        width: '100%',
        padding: 20
    },
    boxStar: {
        marginLeft: 5,
        flexDirection: 'row'
    },
    back: {
        flex: 1,
    }
});


const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

// user.language == 'th' ? item.lang.th.name : user.language == 'en' ? item.lang.en.name : ''

export default connect(
    mapStateToProps,
    { push, goBack }
)(SimilarShopPage)