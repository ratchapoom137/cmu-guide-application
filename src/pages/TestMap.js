import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, Picker } from 'react-native';
import { Icon, Card, Button } from '@ant-design/react-native'
import Loader from '../components/items/Loader'
import IconMat from 'react-native-vector-icons/MaterialCommunityIcons';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconFont5 from 'react-native-vector-icons/FontAwesome5';
import IconFont from 'react-native-vector-icons/FontAwesome';
import IconFeather from 'react-native-vector-icons/Feather';
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import SubHeader from '../components/general/SubHeader'
import axios from 'axios';
import { defaults } from 'lodash';
import {
    Content,
    BodyMain,
    Body,
    Menu,
    ShopNameText,
    ShopItemContent
} from '../components/General.styled'
import {
    ROUTE_SHOP
} from '../constants/RouteConstants'

class ShopListPage extends Component {

    state = {
        shopList: [],
        colorDistanceButton: 'white',
        colorPopularityButton: '#b3b3b3',
        value: 'รวม',
        pickerSelection: this.props.location.state.category,
        loading: true,
        reviews: [],
        totalRate: 0
    }

    componentWillMount() {
        this.renderShopList();
        // console.log("Cate: " + this.props.location.state.category)
    }

    renderShopList = () => {
        axios.get('https://chiangmai.thaimarket.guide/shop?offset=0&limit=0&fbclid=IwAR32UgvD_cRbbC9dQHocuUAcrQ_Ic-pnZ1EGxqGeU_FXnDkStmzcwX1F3OY')
            .then(response => {
                this.setState({
                    shopList: response.data,
                    loading: false
                })
                this.state.shopList.data.map((item, index) => {
                    this.renderReviewListByShopId(item.id, index)
                    // console.log("item: ", item);

                })
                // console.log("Shoplist : ", this.state.shopList.data[1]);

            })
            .catch((error) => {
                console.error('Error: ' + error);
            })
    }

    renderReviewListByShopId = (shopId, indexShop) => {
        axios.get(`http://35.173.241.14:8888/review/shop/${shopId}`)
            .then(response => {
                let sum = 0;
                response.data.map((item, index, arr) => {
                    console.log("RESDASDASD: ", item.deleted);
                    if (item.deleted === false) {
                        console.log("item deketed: ", item);
                        sum = sum + item.rate
                        // console.log("SUM JA: ", sum);
                        if (arr.length - 1 === index) {
                            sum = (sum / arr.length).toFixed(1)
                            // sum = Math.round(sum)
                            this.setState({
                                reviews: [...this.state.reviews, { sum, indexShop }]
                            })
                            this.state.shopList.data[indexShop].status = sum
                            // this.state.reviews.push({sum, indexShop})
                            console.log("Shoplist222 : ", this.state.shopList.data[indexShop]);
                        }
                    }
                })

            })
            .catch((error) => {
                console.error('Error: ' + error);
            })
    }

    onPressDistance = () => {
        this.setState({ colorDistanceButton: 'white' })
        this.setState({ colorPopularityButton: '#bfbfbf' })
    }

    onPressPopularity = () => {
        this.setState({ colorDistanceButton: '#bfbfbf' })
        this.setState({ colorPopularityButton: 'white' })
    }

    onChangeValuePicker = (itemValue) => {
        console.log(itemValue)
        this.setState({ pickerSelection: itemValue })
    }

    navigationToShopPage = (id) => {
        const { push } = this.props
        push(ROUTE_SHOP, { id })
    }

    _keyExtractor = (item, index) => item.id;

    render() {
        // console.log('111111111', this.state.reviews)
        const { user } = this.props
        if (this.state.reviews.length > 0) {
            return (
                <BodyMain>
                    <SubHeader title={user.language == 'th' ? 'ร้านค้า ใน CMU Guide' : user.language == 'en' ? 'Shop in CMU Guide' : ''} hideGoBack={'back'} />
                    <Menu sortBy>
                        <Body menu>
                            <Text>Sort By</Text>
                        </Body>
                        <Body menu style={styles.height}>
                            <Button
                                style={{
                                    backgroundColor: this.state.colorDistanceButton,
                                    borderRadius: 30,
                                    borderColor: this.state.colorPopularityButton,
                                    height: '50%', width: '80%'
                                }}
                                size="small"
                                onPress={() => this.onPressDistance()}
                            >
                                {this.state.colorDistanceButton == 'white' ? <Text>Distance</Text> : <Text style={{ color: '#8c8c8c' }}>Distance</Text>}
                            </Button>
                        </Body>
                        <Body menu style={styles.height}>
                            <Button
                                style={{
                                    backgroundColor: this.state.colorPopularityButton,
                                    borderRadius: 30,
                                    borderColor: this.state.colorDistanceButton,
                                    height: '50%', width: '80%'
                                }}
                                size="small"
                                onPress={() => this.onPressPopularity()}
                            >
                                {this.state.colorPopularityButton == 'white' ? <Text>Popularity</Text> : <Text style={{ color: '#8c8c8c' }}>Popularity</Text>}
                            </Button>
                        </Body>
                        <View style={{ borderRadius: 10, backgroundColor: '#ffb366' }}>
                            <Picker
                                selectedValue={this.state.pickerSelection}
                                style={{ height: 40, width: 110 }}
                                onValueChange={(itemValue) =>
                                    this.setState({ pickerSelection: itemValue })
                                }
                            >
                                <Picker.Item label="รวม" value="รวม" />
                                <Picker.Item label="ร้านค้า" value="ร้านค้า" />
                                <Picker.Item label="ร้านอาหาร (และเครื่องดื่ม)" value="ร้านอาหาร (และเครื่องดื่ม)" />
                                <Picker.Item label="เครื่องประดับ" value="เครื่องประดับ" />
                                <Picker.Item label="คาเฟ่และของหวาน" value="คาเฟ่และของหวาน" />
                                <Picker.Item label="บริการต่างๆ" value="บริการต่างๆ" />
                                <Picker.Item label="อาหารไทย" value="อาหารไทย" />
                                <Picker.Item label="อาหารเอเชีย" value="อาหารเอเชีย" />
                                <Picker.Item label="สินค้าเบ็ดเตล็ด และอื่นๆ" value="สินค้าเบ็ดเตล็ด และอื่นๆ" />
                                <Picker.Item label="เฟอร์นิเจอร์และของตกแต่งบ้าน" value="เฟอร์นิเจอร์และของตกแต่งบ้าน" />
                                <Picker.Item label="ของที่ระลึก" value="ของที่ระลึก" />
                                <Picker.Item label="ศูนย์การค้า" value="ศูนย์การค้า" />
                                <Picker.Item label="ต้นไม้และอุปกรณ์ทำสวน" value="ต้นไม้และอุปกรณ์ทำสวน" />
                                <Picker.Item label="ของเก่า ของสะสม" value="ของเก่า ของสะสม" />
                                <Picker.Item label="สุขภาพและความงาม" value="สุขภาพและความงาม" />
                                <Picker.Item label="ตลาด" value="ตลาด" />
                                <Picker.Item label="แฟชั่นสตรี" value="แฟชั่นสตรี" />
                                <Picker.Item label="แฟชั่นบุรุษ" value="แฟชั่นบุรุษ" />
                            </Picker>
                        </View>
                    </Menu>
                    <Content>
                        {
                            this.state.loading === false ? (
                                <FlatList
                                    style={{ width: '100%' }}
                                    numColumns={1}
                                    data={this.state.shopList.data}
                                    keyExtractor={this._keyExtractor}
                                    renderItem={(item, index) => item.item.category == this.state.pickerSelection || this.state.pickerSelection == 'รวม' ? (
                                        <Card full>
                                            <Card.Header
                                                title={user.language == 'th' ? <View style={{ width: 250 }}><ShopNameText numberOfLines={1}>{item.item.lang.th.name}</ShopNameText></View> : user.language == 'en' ? <ShopNameText>{item.item.lang.en.name}</ShopNameText> : ''}
                                                thumb={
                                                    item.item.category == 'ร้านค้า' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/shop.png')} /> :
                                                        item.item.category == 'ร้านอาหาร (และเครื่องดื่ม)' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/food-drink.png')} /> :
                                                            item.item.category == 'เครื่องประดับ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/accessories.png')} /> :
                                                                item.item.category == 'คาเฟ่และของหวาน' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/cafe.png')} /> :
                                                                    item.item.category == 'บริการต่างๆ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/service.png')} /> :
                                                                        item.item.category == 'อาหารไทย' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/thaiFood.png')} /> :
                                                                            item.item.category == 'อาหารเอเชีย' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/noodles.png')} /> :
                                                                                item.item.category == 'สินค้าเบ็ดเตล็ด และอื่นๆ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/more-item.png')} /> :
                                                                                    item.item.category == 'เฟอร์นิเจอร์และของตกแต่งบ้าน' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/forniture.png')} /> :
                                                                                        item.item.category == 'ของที่ระลึก' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/gift.png')} /> :
                                                                                            item.item.category == 'ศูนย์การค้า' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/mall.png')} /> :
                                                                                                item.item.category == 'ต้นไม้และอุปกรณ์ทำสวน' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/tree-icon.png')} /> :
                                                                                                    item.item.category == 'ของเก่า ของสะสม' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/collections.png')} /> :
                                                                                                        item.item.category == 'สุขภาพและความงาม' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/heart.png')} /> :
                                                                                                            item.item.category == 'ตลาด' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/market-icon.png')} /> :
                                                                                                                item.item.category == 'แฟชั่นสตรี' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/fashion-women.png')} /> :
                                                                                                                    item.item.category == 'แฟชั่นบุรุษ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/fashion-men.png')} /> : <Icon style={{ right: 8 }} name="file-unknown" size={20} />
                                                }
                                                extra={
                                                    <View style={{ alignItems: 'flex-end' }}>
                                                        <TouchableOpacity style={{ paddingHorizontal: 10 }}>
                                                            <IconMat name="crosshairs-gps" size={24} />
                                                        </TouchableOpacity>
                                                    </View>}
                                            />
                                            <TouchableOpacity onPress={() => { this.navigationToShopPage(item.item.id) }}>
                                                <Card.Body>
                                                    <ShopItemContent>
                                                        <Image style={{ width: 140, height: 160, resizeMode: 'contain' }} source={{ uri: item.item.image }} />
                                                        <View style={styles.width}>
                                                            <ShopItemContent>
                                                                <View style={styles.border}>
                                                                    <TouchableOpacity style={styles.menuButton} >
                                                                        <IconFont style={{ marginStart: 5 }} name="star" size={18} color='#ffb31a' />
                                                                        <View style={{ margin: 5 }}>
                                                                            <Text>
                                                                                {item.item.status != "ACTIVE" ? item.item.status : '0.0'}
                                                                            </Text>
                                                                        </View>
                                                                    </TouchableOpacity>
                                                                </View>
                                                                <View style={styles.border}>
                                                                    <TouchableOpacity style={styles.menuButton} >
                                                                        <IconFont5 style={{ marginStart: 5 }} name="map-marker-alt" size={18} color='#999999' />
                                                                        <View style={{ margin: 5 }}><Text>5 km.</Text></View>
                                                                    </TouchableOpacity>
                                                                </View>
                                                                {item.item.highlighted == true ? <IconMat style={{ margin: 4 }} name="trophy-award" size={20} color="#9900cc" /> : console.log()}
                                                            </ShopItemContent>
                                                            <View style={{ width: '60%' }}>
                                                                <Text numberOfLines={5}>
                                                                    {user.language == 'th' ? item.item.lang.th.description : user.language == 'en' ? item.item.lang.en.description : ''}
                                                                </Text>
                                                            </View>
                                                        </View>
                                                    </ShopItemContent>
                                                </Card.Body>
                                                {/* <Card.Footer content="footer content" extra="footer extra content" /> */}
                                            </TouchableOpacity>
                                        </Card>
                                    ) : console.log()}
                                />
                            ) : (
                                    <Loader
                                        modalVisible={this.state.loading}
                                        animationType="fade"
                                    />
                                )
                        }
                    </Content>

                </BodyMain>
            )
        } else {
            return (
                <BodyMain>
                    <Loader
                        modalVisible={this.state.loading}
                        animationType="fade"
                    />
                </BodyMain>
            )
        }
    }

}

const styles = StyleSheet.create({
    menuButton: {
        borderRadius: 8,
        margin: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    border: {
        backgroundColor: '#cccccc',
        borderRadius: 8,
        margin: 2
    },
    width: {
        width: '100%'
    },
    height: {
        height: '100%'
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    { push }
)(ShopListPage) 