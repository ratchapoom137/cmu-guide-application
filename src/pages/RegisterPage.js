import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Alert, Image } from 'react-native';
import { InputItem, WhiteSpace, Icon, Button } from '@ant-design/react-native';
import { push, goBack } from 'connected-react-router'
import { connect } from 'react-redux'
import axios from 'axios';
import {
    Body,
    SignupTextCont,
    SignupText,
    SignupButton,
    LoginButton,
    Center
} from '../components/General.styled'
import {
    ROUTE_LOGIN
} from '../constants/RouteConstants'

class RegisterPage extends Component {

    state = {
        email: '',
        password: '',
        confirmPassword: '',
        firstname: '',
        lastname: '',
    }

    navigateToLoginPage = () => {
        this.props.history.push('/login')
    }

    onRegister = () => {
        const { user } = this.props
        if (this.state.email === '' || this.state.password === '' || this.state.confirmPassword === '' || this.state.firstname === '' || this.state.lastname === '') {
            Alert.alert('Please do not leave input fields blanked')
        } else {
            if (this.state.password !== this.state.confirmPassword) {
                Alert.alert('Incorrect Input', 'Confirmed password is not matched.')
            } else {
                try {
                    axios.post('http://35.173.241.14:8888/user/register', {
                        email: this.state.email,
                        password: this.state.password,
                        firstName: this.state.firstname,
                        lastName: this.state.lastname
                    }).then(response => {
                        if (user.language === 'th') {
                            Alert.alert(
                                'ลงทะเบียนสำเร็จ',
                                'ขอบคุณที่ใช้บริการ',
                                [{ text: 'กลับไปเข้าสู่ระบบ', onPress: () => { this.props.push(ROUTE_LOGIN) } }]
                            );
                        } else {
                            Alert.alert(
                                'Sign up Sucessed',
                                'Thank you',
                                [{ text: 'Sign in', onPress: () => { this.props.push(ROUTE_LOGIN) } }]
                            );
                        }
                    })
                } catch (error) {
                    Alert.alert('สมัครสมาชิกไม่สำเร็จ')
                }
                // axios({
                //     url: 'http://35.173.241.14:8888/user/register',
                //     method: 'post',
                //     data: {
                //         email: this.state.email,
                //         password: this.state.password,
                //         firstName: this.state.firstname,
                //         lastName: this.state.lastname
                //     }
                // }).then(res => {
                //     alert('ressssd ' + res.data)
                //     console.log("qwqwoeqweopqwe", res.data);

                // }).catch(e => {
                //     if (e.response.status === 422) {
                //         alert('This email is use registed')
                //     } else {
                //         alert('Email or password ' + e.response.data.errors)
                //     }
                // })
            }
        }
    }

    render() {
        const { user, goBack } = this.props
        console.log("poom" + this.props.user)
        return (
            <Body>
                <View style={{ width: '100%' }}>
                    <TouchableOpacity onPress={goBack} style={{ marginLeft: 15, marginTop: 15 }}>
                        {/* <Text style={{ fontSize: 20}}>{user.language == 'th' ? 'กลับ' : user.language == 'en' ? 'Back' : ''}</Text> */}
                        <Icon name="left" color="#ffcc99" size={30} />
                    </TouchableOpacity>
                </View>
                <SignupTextCont>
                    <View style={{ backgroundColor: "rgba(255,255,255,0.3)", alignItems: "center", justifyContent: 'center', padding: 24 }}>
                        <View style={{ margin: 10 }}>
                            <Text style={styles.textTitle}>{user.language == 'th' ? 'สมัครสมาชิก' : user.language == 'en' ? 'Register' : ''}</Text>
                        </View>
                        <View style={{ width: 300 }}>
                            <InputItem
                                clear
                                value={this.state.email}
                                onChange={value => { this.setState({ email: value }) }}
                                placeholder="Email"
                            >
                                <Icon name="user" size="md" color="white" />
                            </InputItem>
                            <InputItem
                                clear
                                type="password"
                                secureTextEntry={true}
                                value={this.state.password}
                                onChange={value => { this.setState({ password: value }) }}
                                placeholder="Password"
                            >
                                <Icon name="lock" size="md" color="white" />
                            </InputItem>
                            <InputItem
                                clear
                                type="password"
                                secureTextEntry={true}
                                value={this.state.confirmPassword}
                                onChange={value => { this.setState({ confirmPassword: value }) }}
                                placeholder="Confirm Password"
                            >
                                <Icon name="unlock" size="md" color="white" />
                            </InputItem>
                            <InputItem
                                clear
                                value={this.state.firstname}
                                onChange={value => { this.setState({ firstname: value }) }}
                                placeholder="First name"
                            >
                                <Icon name="profile" size="md" color="white" />
                            </InputItem>
                            <InputItem
                                clear
                                value={this.state.lastname}
                                onChange={value => { this.setState({ lastname: value }) }}
                                placeholder="Last name"
                            >
                                <Icon name="profile" size="md" color="white" />
                            </InputItem>
                        </View>
                    </View>
                </SignupTextCont>
                <Center>
                    <Button style={{ width: 140, backgroundColor: '#ff9900', borderRadius: 50, borderColor: '#ff9900' }} onPress={() => this.onRegister()}>
                        <Text style={{ color: 'white' }}>{user.language == 'th' ? 'ยืนยัน' : user.language == 'en' ? 'Submit' : ''}</Text>
                    </Button>
                </Center>
                <WhiteSpace />
                <WhiteSpace />
                <WhiteSpace />
                <Text style={{ color: 'gray' }}>───────────────────────────</Text>
                <SignupTextCont>
                    <SignupText>If you have an account!</SignupText>
                    <TouchableOpacity onPress={() => { this.navigateToLoginPage() }}><SignupButton> Signin</SignupButton></TouchableOpacity>
                </SignupTextCont>
            </Body>
        );
    }
}

const styles = StyleSheet.create({
    textTitle: {
        color: '#ffffff',
        fontSize: 24,
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, { push, goBack })(RegisterPage)