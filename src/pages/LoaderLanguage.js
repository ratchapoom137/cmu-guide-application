import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity, ImageBackground, Animated, Button } from 'react-native';
import { TabBar, Icon, Carousel } from '@ant-design/react-native'
import { goBack } from 'connected-react-router'
import { connect } from 'react-redux'
import Loader from '../components/items/Loader'
import { push } from 'connected-react-router'
import Header from '../components/general/Header'
import {
    BodyMain,
    Content,
    ImageCarousel,
    TextCarousel,
    BoxTextCarousel,
    CarouselBox,
    Menu,
    ItemMenu,
    MenuStyle,
} from '../components/General.styled'
import {
    ROUTE_FOOTER
} from '../constants/RouteConstants'

class SplashScreen extends Component {

    state = {
        loading: false
    }

    componentDidMount() {
        const { push } = this.props
        this.setState({ loading: true})
        setTimeout(() => {
            this.setState({loading: false})
            push(ROUTE_FOOTER)
        }, 1500)
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {/* <Loader
                    modalVisible={this.state.loading}
                    animationType="fade"
                /> */}
            </View>
        )
    }
}

export default connect(
    null,
    { push }
)(SplashScreen)