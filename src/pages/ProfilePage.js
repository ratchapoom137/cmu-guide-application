import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { WingBlank, WhiteSpace, Button } from '@ant-design/react-native'
import { goBack } from 'connected-react-router'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import Header from '../components/general/Header'
import IconMat from 'react-native-vector-icons/MaterialIcons';
import axios from 'axios';
import {
    ROUTE_LOGIN,
    ROUTE_REGISTER,
    ROUTE_REVIEWHISTORY,
    ROUTE_TRIP
} from '../constants/RouteConstants'
import {
    Content,
    SignupTextCont,
    SignupText,
    SignupButton,
    LoginButton
} from '../components/General.styled'

const PlaceHolder = props => (
    <View
        style={{
            backgroundColor: '#eee',
            height: 3,
            width: 230,
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 500
        }}
        {...props}
    >
    </View>
);

const PlaceHolder2 = props => (
    <View
        style={{
            backgroundColor: '#eee',
            height: 3,
            width: 320,
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 500
        }}
        {...props}
    >
    </View>
);

class ProfilePage extends Component {

    state = {
        pathName: '',
        numberOfReview: 0
    }

    componentWillMount() {
        if (this.props.user.information) {
            this.getReviewByCustomerId()
        }
    }

    onPressLogin = () => {
        const { push } = this.props
        push(ROUTE_LOGIN)
    }

    navigationToRegisterPage = () => {
        const { push } = this.props
        push(ROUTE_REGISTER)
    }

    onPressReviewHistory = () => {
        const { push } = this.props
        push(ROUTE_REVIEWHISTORY)
    }

    getReviewByCustomerId = () => {
        const { user } = this.props
        axios({
            url: `http://35.173.241.14:8888/review/user/${user.information.user.id}`,
            method: 'get',
            headers: { "Authorization": `Bearer ${user.information.token}` }

        }).then(response => {
            if (response.data.length > 0) {
                this.setState({
                    numberOfReview: response.data.length
                })
            }
            console.log("num", this.state.numberOfReview);

        }).catch(e => {
            alert('not successed' + e.response.data)
            console.log('res data: ', e.response.data);
            console.log('res data: ', e.response);
        })
    }

    onPressMyTrip = () => {
        this.props.push(ROUTE_TRIP)
    }

    render() {
        const { user } = this.props
        if (user.information != null) {
            return (
                <View>
                    <Header title='PROFILE' />
                    <Content>
                        <View>
                            {
                                // user.information.user.image == null ? (
                                //     <Image style={{ width: 150, height: 150, borderRadius: 150 / 2 }} source={require('../assets/image/imageUser2.png')} />
                                // ) : user.information == null ? null : (
                                //     <Image style={{ width: 150, height: 150, borderRadius: 150 / 2 }} source={{ uri: user.information.user.image }} />
                                // )

                                user.information == null ? null : user.information.user.image == null ? (
                                    <Image style={{ width: 150, height: 150, borderRadius: 150 / 2 }} source={require('../assets/image/imageUser2.png')} />
                                ) : <Image style={{ width: 150, height: 150, borderRadius: 150 / 2 }} source={{ uri: user.information.user.image }} />

                            }
                            <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => this.onPressReviewHistory()}>
                                <IconMat name="rate-review" size={26} />
                                <Text style={{ fontSize: 20, color: 'black' }}>
                                    {user.language == 'th' ? 'ประวัติการรีวิว' : user.language == 'en' ? 'Review History' : ''} : {this.state.numberOfReview}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <WingBlank style={{ marginTop: 15, marginBottom: 15 }} size="sm">
                            <PlaceHolder2 />
                        </WingBlank>
                        <View>
                            <Text>First Name: {user.information.user.firstName}</Text>
                            <Text>Last Name: {user.information.user.lastName}</Text>
                        </View>
                        <WingBlank style={{ marginTop: 15, marginBottom: 15 }} size="sm">
                            <PlaceHolder2 />
                        </WingBlank>
                        <Button style={{ width: '50%', backgroundColor: '#ff9900', borderRadius: 50 }} onPress={() => this.onPressMyTrip()}>
                            <Text style={{ color: 'white' }}>{user.language == 'th' ? 'ทริปของฉัน' : user.language == 'en' ? 'My Trip' : ''}</Text>
                        </Button>
                    </Content>
                </View>
            )
        } else {
            return (
                <View>
                    <Header title='PROFILE' />
                    <Content>
                        <View style={{ height: 540, alignItems: 'center', justifyContent: 'center' }}>
                            {/* <Button onPress={() => this.onPressLogin()}>Log In</Button> */}
                            <Button style={{ width: '70%', backgroundColor: '#ff9900', borderRadius: 50, marginBottom: 10 }} onPress={() => this.onPressLogin()}>
                                <Text style={{ color: 'white' }}>{user.language == 'th' ? 'เข้าสู่ระบบ' : user.language == 'en' ? 'Login' : ''}</Text>
                            </Button>
                            <WingBlank size="sm">
                                <PlaceHolder />
                            </WingBlank>
                            <View style={{ marginTop: 15, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Text>{user.language == 'th' ? 'ยังไม่มีบัญชีใช่ไหม?' : user.language == 'en' ? 'Dont have an account yet?' : ''}</Text>
                                <TouchableOpacity style={{ marginLeft: 5 }} onPress={() => { this.navigationToRegisterPage() }}>
                                    <Text style={{ color: '#ff9900', fontWeight: '500', fontSize: 18 }}>{user.language == 'th' ? 'ลงทะเบียน' : user.language == 'en' ? 'Signup' : ''}</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </Content>
                </View>
            )
        }
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, { push })(ProfilePage)