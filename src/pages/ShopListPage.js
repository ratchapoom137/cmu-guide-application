import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, Picker, ToastAndroid, Alert } from 'react-native';
import { Icon, Card, Button } from '@ant-design/react-native'
import Loader from '../components/items/Loader'
import IconMat from 'react-native-vector-icons/MaterialCommunityIcons';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconFont5 from 'react-native-vector-icons/FontAwesome5';
import IconFont from 'react-native-vector-icons/FontAwesome';
import IconFeather from 'react-native-vector-icons/Feather';
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import SubHeader from '../components/general/SubHeader'
import axios from 'axios';
import _ from 'lodash';
import {
    Content,
    BodyMain,
    Body,
    Menu,
    ShopNameText,
    ShopItemContent
} from '../components/General.styled'
import {
    ROUTE_SHOP,
    ROUTE_LOGIN
} from '../constants/RouteConstants'

class ShopListPage extends Component {

    state = {
        shopList: [],
        colorDistanceButton: 'white',
        colorPopularityButton: '#b3b3b3',
        value: 'รวม',
        pickerSelection: this.props.location.state.category,
        loading: true,
        reviews: [],
        totalRate: 0,
        check: 0
    }

    componentWillMount() {
        this.renderShopList();
        this.calculateAverageStar();
        console.log("test norrr", this.props.location.state);

    }

    renderShopList = () => {
        axios.get('https://chiangmai.thaimarket.guide/shop?offset=0&limit=0&fbclid=IwAR32UgvD_cRbbC9dQHocuUAcrQ_Ic-pnZ1EGxqGeU_FXnDkStmzcwX1F3OY')
            .then(response => {
                this.setState({
                    shopList: response.data.data,
                    loading: false
                })
                this.state.shopList.map((item, index) => {
                    this.renderReviewListByShopId(item.id, index)
                })

            })
            .catch((error) => {
                console.error('Error: ' + error);
            })
    }

    renderReviewListByShopId = (shopId, indexShop) => {
        axios.get(`http://35.173.241.14:8888/review/shop/${shopId}`)
            .then(response => {
                let sum = 0;
                response.data.map((item, index, arr) => {
                    // console.log('itemmmm; ', item);

                    if (item.deleted === false) {
                        this.state.reviews.push(item)
                        console.log("sseieieieieiei", this.state.reviews);
                        this.calculateAverageStar()
                    }
                })
            })
            .catch((error) => {
                console.error('Error: ' + error);
            })

    }

    calculateAverageStar = () => {
        const test = _.map(this.state.shopList, (s) => {
            const shopStar = _.filter(this.state.reviews, { 'shopId': s.id })
            const shopSum = _.sumBy(shopStar, (o) => {
                return o.rate
            })
            console.log('shopSummm', shopSum);
            if (shopStar.length > 0) {
                return {
                    ...s,
                    averageRate: shopSum / shopStar.length
                }
            } else {
                return {
                    ...s,
                    averageRate: 0
                }
            }

        })
        // console.log("TEst : ", test);
        this.setState({ shopList: test })
    }

    onPressDistance = () => {
        this.setState({ colorDistanceButton: 'white' })
        this.setState({ colorPopularityButton: '#bfbfbf' })
    }

    onPressPopularity = () => {
        this.setState({ colorDistanceButton: '#bfbfbf' })
        this.setState({ colorPopularityButton: 'white' })
    }

    onChangeValuePicker = (itemValue) => {
        console.log(itemValue)
        this.setState({ pickerSelection: itemValue })
    }

    navigationToShopPage = (id) => {
        const { push } = this.props
        push(ROUTE_SHOP, { id })
    }

    onPressForAddToTrip = (shopId) => {
        const { user, push } = this.props
        if (user.information) {
            axios({
                url: `http://35.173.241.14:8888/trip/add/triporder/${this.props.location.state.tripId}`,
                method: 'post',
                headers: { "Authorization": `Bearer ${user.information.token}` },
                data: {
                    shopId: shopId
                }
            }).then(res => {
                if(user.language === 'th') {
                    // Toast.success('บันทึกลงในทริปสำเร็จ')
                    // ToastAndroid.show('บันทึกลงในทริปสำเร็จ!', ToastAndroid.SHORT);
                    ToastAndroid.showWithGravity(
                        'บันทึกลงในทริปสำเร็จ',
                        ToastAndroid.SHORT,
                        ToastAndroid.CENTER,
                      );
                } else {
                    // Toast.success('Save to a successful trip')
                    // ToastAndroid.show('Save to a successful trip!', ToastAndroid.SHORT);
                    ToastAndroid.showWithGravity(
                        'Save to a successful trip!',
                        ToastAndroid.SHORT,
                        ToastAndroid.CENTER,
                      );
                }
            }).catch(e => {
                alert('not successed' + e.response.data)
                console.log('res data: ', e.response.data);
                console.log('res data: ', e.response);
            })
        } else {
            if(user.language === 'th') {
                Alert.alert(
                    'โปรดลงชื่อเข้าใช้หรือสมัครการใช้งานบัญชีใหม่',
                    '',
                    [{ text: 'ไปหน้าลงชื่อเข้าใช้', onPress: () => { this.props.push(ROUTE_LOGIN) } }]
                );
            } else {
                Alert.alert(
                    'Please sign in or sign up',
                    '',
                    [{ text: 'Signin', onPress: () => { this.props.push(ROUTE_LOGIN) } }]
                );
            }
            
            
        }
    }

    _keyExtractor = (item, index) => item.id;

    render() {
        const { user } = this.props
        if (this.state.reviews) {
            return (
                <BodyMain>
                    <SubHeader title={user.language == 'th' ? 'ร้านค้า ใน CMU Guide' : user.language == 'en' ? 'Shop in CMU Guide' : ''} hideGoBack={'back'} />
                    <Menu sortBy>
                        <Body menu>
                            <Text>Sort By</Text>
                        </Body>
                        <Body menu style={styles.height}>
                            <Button
                                style={{
                                    backgroundColor: this.state.colorDistanceButton,
                                    borderRadius: 30,
                                    borderColor: this.state.colorPopularityButton,
                                    height: '50%', width: '80%'
                                }}
                                size="small"
                                onPress={() => this.onPressDistance()}
                            >
                                {this.state.colorDistanceButton == 'white' ? <Text>Distance</Text> : <Text style={{ color: '#8c8c8c' }}>Distance</Text>}
                            </Button>
                        </Body>
                        <Body menu style={styles.height}>
                            <Button
                                style={{
                                    backgroundColor: this.state.colorPopularityButton,
                                    borderRadius: 30,
                                    borderColor: this.state.colorDistanceButton,
                                    height: '50%', width: '80%'
                                }}
                                size="small"
                                onPress={() => this.onPressPopularity()}
                            >
                                {this.state.colorPopularityButton == 'white' ? <Text>Popularity</Text> : <Text style={{ color: '#8c8c8c' }}>Popularity</Text>}
                            </Button>
                        </Body>
                        <View style={{ borderRadius: 10, backgroundColor: '#ffb366' }}>
                            <Picker
                                selectedValue={this.state.pickerSelection}
                                style={{ height: 40, width: 110 }}
                                onValueChange={(itemValue) =>
                                    this.setState({ pickerSelection: itemValue })
                                }
                            >
                                <Picker.Item label="รวม" value="รวม" />
                                <Picker.Item label="ร้านค้า" value="ร้านค้า" />
                                <Picker.Item label="ร้านอาหาร (และเครื่องดื่ม)" value="ร้านอาหาร (และเครื่องดื่ม)" />
                                <Picker.Item label="เครื่องประดับ" value="เครื่องประดับ" />
                                <Picker.Item label="คาเฟ่และของหวาน" value="คาเฟ่และของหวาน" />
                                <Picker.Item label="บริการต่างๆ" value="บริการต่างๆ" />
                                <Picker.Item label="อาหารไทย" value="อาหารไทย" />
                                <Picker.Item label="อาหารเอเชีย" value="อาหารเอเชีย" />
                                <Picker.Item label="สินค้าเบ็ดเตล็ด และอื่นๆ" value="สินค้าเบ็ดเตล็ด และอื่นๆ" />
                                <Picker.Item label="เฟอร์นิเจอร์และของตกแต่งบ้าน" value="เฟอร์นิเจอร์และของตกแต่งบ้าน" />
                                <Picker.Item label="ของที่ระลึก" value="ของที่ระลึก" />
                                <Picker.Item label="ศูนย์การค้า" value="ศูนย์การค้า" />
                                <Picker.Item label="ต้นไม้และอุปกรณ์ทำสวน" value="ต้นไม้และอุปกรณ์ทำสวน" />
                                <Picker.Item label="ของเก่า ของสะสม" value="ของเก่า ของสะสม" />
                                <Picker.Item label="สุขภาพและความงาม" value="สุขภาพและความงาม" />
                                <Picker.Item label="ตลาด" value="ตลาด" />
                                <Picker.Item label="แฟชั่นสตรี" value="แฟชั่นสตรี" />
                                <Picker.Item label="แฟชั่นบุรุษ" value="แฟชั่นบุรุษ" />
                            </Picker>
                        </View>
                    </Menu>
                    <Content>
                        {
                            this.state.loading === false ? (
                                <FlatList
                                    style={{ width: '100%' }}
                                    numColumns={1}
                                    data={this.state.shopList}
                                    keyExtractor={this._keyExtractor}
                                    renderItem={(item, index) => item.item.category == this.state.pickerSelection || this.state.pickerSelection == 'รวม' ? (
                                        <Card full>
                                            <Card.Header
                                                title={user.language == 'th' ? <View style={{ width: 250 }}><ShopNameText numberOfLines={1}>{item.item.lang.th.name}</ShopNameText></View> : user.language == 'en' ? <ShopNameText>{item.item.lang.en.name}</ShopNameText> : ''}
                                                thumb={
                                                    item.item.category == 'ร้านค้า' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/shop.png')} /> :
                                                        item.item.category == 'ร้านอาหาร (และเครื่องดื่ม)' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/food-drink.png')} /> :
                                                            item.item.category == 'เครื่องประดับ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/accessories.png')} /> :
                                                                item.item.category == 'คาเฟ่และของหวาน' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/cafe.png')} /> :
                                                                    item.item.category == 'บริการต่างๆ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/service.png')} /> :
                                                                        item.item.category == 'อาหารไทย' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/thaiFood.png')} /> :
                                                                            item.item.category == 'อาหารเอเชีย' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/noodles.png')} /> :
                                                                                item.item.category == 'สินค้าเบ็ดเตล็ด และอื่นๆ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/more-item.png')} /> :
                                                                                    item.item.category == 'เฟอร์นิเจอร์และของตกแต่งบ้าน' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/forniture.png')} /> :
                                                                                        item.item.category == 'ของที่ระลึก' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/gift.png')} /> :
                                                                                            item.item.category == 'ศูนย์การค้า' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/mall.png')} /> :
                                                                                                item.item.category == 'ต้นไม้และอุปกรณ์ทำสวน' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/tree-icon.png')} /> :
                                                                                                    item.item.category == 'ของเก่า ของสะสม' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/collections.png')} /> :
                                                                                                        item.item.category == 'สุขภาพและความงาม' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/heart.png')} /> :
                                                                                                            item.item.category == 'ตลาด' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/market-icon.png')} /> :
                                                                                                                item.item.category == 'แฟชั่นสตรี' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/fashion-women.png')} /> :
                                                                                                                    item.item.category == 'แฟชั่นบุรุษ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/fashion-men.png')} /> : <Icon style={{ right: 8 }} name="file-unknown" size={20} />
                                                }
                                                extra={
                                                    <View style={{ alignItems: 'flex-end' }}>
                                                        <TouchableOpacity style={{ paddingHorizontal: 10 }} onPress={() => this.onPressForAddToTrip(item.item.id)}>
                                                            <IconMat name="crosshairs-gps" size={24} />
                                                        </TouchableOpacity>
                                                    </View>}
                                            />
                                            <TouchableOpacity onPress={() => { this.navigationToShopPage(item.item.id) }}>
                                                <Card.Body>
                                                    <ShopItemContent>
                                                        <Image style={{ width: 140, height: 160, resizeMode: 'contain' }} source={{ uri: item.item.image }} />
                                                        <View style={styles.width}>
                                                            <ShopItemContent>
                                                                <View style={styles.border}>
                                                                    <TouchableOpacity style={styles.menuButton} >
                                                                        <IconFont style={{ marginStart: 5 }} name="star" size={18} color='#ffb31a' />
                                                                        <View style={{ margin: 5 }}>
                                                                            <Text>
                                                                                {/* {item.item.status != "ACTIVE" ? item.item.status : '0.0'} */}
                                                                                {item.item.averageRate}
                                                                            </Text>
                                                                        </View>
                                                                    </TouchableOpacity>
                                                                </View>
                                                                <View style={styles.border}>
                                                                    <TouchableOpacity style={styles.menuButton} >
                                                                        <IconFont5 style={{ marginStart: 5 }} name="map-marker-alt" size={18} color='#999999' />
                                                                        <View style={{ margin: 5 }}><Text>5 km.</Text></View>
                                                                    </TouchableOpacity>
                                                                </View>
                                                                {item.item.highlighted == true ? <IconMat style={{ margin: 4 }} name="trophy-award" size={20} color="#9900cc" /> : console.log()}
                                                            </ShopItemContent>
                                                            <View style={{ width: '60%' }}>
                                                                <Text numberOfLines={5}>
                                                                    {user.language == 'th' ? item.item.lang.th.description : user.language == 'en' ? item.item.lang.en.description : ''}
                                                                </Text>
                                                            </View>
                                                        </View>
                                                    </ShopItemContent>
                                                </Card.Body>
                                                {/* <Card.Footer content="footer content" extra="footer extra content" /> */}
                                            </TouchableOpacity>
                                        </Card>
                                    ) : console.log()}
                                />
                            ) : (
                                    <Loader
                                        modalVisible={this.state.loading}
                                        animationType="fade"
                                    />
                                )
                        }
                    </Content>

                </BodyMain>
            )
        } else {
            return (
                <BodyMain>
                    <Loader
                        modalVisible={this.state.loading}
                        animationType="fade"
                    />
                </BodyMain>
            )
        }
    }

}

const styles = StyleSheet.create({
    menuButton: {
        borderRadius: 8,
        margin: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    border: {
        backgroundColor: '#cccccc',
        borderRadius: 8,
        margin: 2
    },
    width: {
        width: '100%'
    },
    height: {
        height: '100%'
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    { push }
)(ShopListPage)