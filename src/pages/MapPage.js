import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Text,
    Image
} from 'react-native';
import { Icon, Button, Card, Modal, Provider } from '@ant-design/react-native'
import IconMat from 'react-native-vector-icons/MaterialCommunityIcons';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconFont5 from 'react-native-vector-icons/FontAwesome5';
import IconFont from 'react-native-vector-icons/FontAwesome';
import IconFeather from 'react-native-vector-icons/Feather';
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import axios from 'axios';
import _ from 'lodash';
import {
    ShopNameText,
    ShopItemContent,
    Row,
    TextCustomer
} from '../components/General.styled'
import {
    ROUTE_SHOP
} from '../constants/RouteConstants'
import shopIcon from '../assets/image/shop-icon-map2.png'
import currentIcon from '../assets/image/currentLocation-icon.png'
import MapView, { Marker } from 'react-native-maps';

class MapPage extends Component {

    state = {
        shopList: [],
        latitude: 18.802457293897053,
        longitude: 98.95544133600833,
        latitudeCurrentLocation: 0,
        longitudeCurrentLocation: 0,
        visibleMarkShop: false,
        visibleInfo: false,
        shop: [],
        visibleCurrentlocation: true,
        reviews:[]
    }

    componentDidMount() {
        this.renderShopList()
    }

    renderCurrentLocation = () => {
        navigator.geolocation.getCurrentPosition(position => {
            this.setState({
                // visibleCurrentlocation: true,
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                error: null,
            });
        });
    }

    renderShopList = () => {
        axios.get('https://chiangmai.thaimarket.guide/shop?offset=0&limit=0&fbclid=IwAR32UgvD_cRbbC9dQHocuUAcrQ_Ic-pnZ1EGxqGeU_FXnDkStmzcwX1F3OY')
            .then(response => {
                this.setState({
                    shopList: response.data.data,
                })
                this.state.shopList.map((item, index) => {
                    this.renderReviewListByShopId(item.id, index)
                })

            })
            .catch((error) => {
                console.error('Error: ' + error);
            })
    }

    renderReviewListByShopId = (shopId, indexShop) => {
        axios.get(`http://35.173.241.14:8888/review/shop/${shopId}`)
            .then(response => {
                let sum = 0;
                response.data.map((item, index, arr) => {
                    // console.log('itemmmm; ', item);

                    if (item.deleted === false) {
                        this.state.reviews.push(item)
                        console.log("sseieieieieiei", this.state.reviews);
                        this.calculateAverageStar()
                    }
                })
            })
            .catch((error) => {
                console.error('Error: ' + error);
            })
            
    }

    calculateAverageStar = () => {
        const test = _.map(this.state.shopList, (s) => {
            const shopStar = _.filter(this.state.reviews, { 'shopId': s.id })
            const shopSum = _.sumBy(shopStar, (o) => {
                return o.rate
            })
            console.log('shopSummm', shopSum);
            if (shopStar.length > 0) {
                return {
                    ...s,
                    averageRate: shopSum / shopStar.length
                }
            } else {
                return {
                    ...s,
                    averageRate: 0
                }
            }

        })
        // console.log("TEst : ", test);
        this.setState({ shopList: test })
    }

    onClickMarkerShop = (item) => {
        this.setState({ latitude: parseFloat(item.location.latitude), longitude: parseFloat(item.location.longitude) })
        if (this.state.visibleMarkShop == true) {
            this.setState({ shop: item })
        } else {
            this.setState({ visibleMarkShop: !this.state.visibleMarkShop, shop: item })
        }
    }

    onCloseModalShop = () => {
        this.setState({ visibleMarkShop: false })
    }

    navigationToShopPage = (id) => {
        const { push } = this.props
        push(ROUTE_SHOP, { id })
    }

    onCloseModalInfo = () => {
        this.setState({ visibleInfo: false })
    }

    onClickInfoButton = () => {
        this.setState({ visibleInfo: true })
    }

    render() {
        const { user } = this.props
        if (this.state.shopList) {
            return (
                <View style={styles.container}>
                    <Provider>
                        <Modal
                            title={user.language == 'th' ? 'สัญลักษณ์' : user.language == 'en' ? 'Symbol' : ''}
                            transparent
                            onClose={() => this.onCloseModalInfo()}
                            maskClosable
                            visible={this.state.visibleInfo}
                            closable
                        >
                            <View>
                                <Row style={{ margin: 20, alignItems: 'center' }}>
                                    <Image source={require('../assets/image/shop-icon-map.png')} style={{ width: 40, height: 40, marginRight: 30 }} />
                                    <TextCustomer infoModal>{user.language == 'th' ? 'ร้านค้า' : user.language == 'en' ? 'Shop' : ''}</TextCustomer>
                                </Row>
                            </View>
                        </Modal>
                    </Provider>
                    <MapView
                        style={{ position: 'relative' }}
                        style={styles.map}
                        showsUserLocation={this.state.visibleCurrentlocation}
                        region={{
                            latitude: this.state.latitude,
                            longitude: this.state.longitude,
                            latitudeDelta: 0.015,
                            longitudeDelta: 0.0121
                        }}
                        onPress={() => this.onCloseModalShop()}
                    >
                        {/* {
                            this.state.latitudeCurrentLocation != 0 && this.state.longitudeCurrentLocation != 0 ?
                                <Marker
                                    coordinate={{
                                        latitude: this.state.latitudeCurrentLocation,
                                        longitude: this.state.longitudeCurrentLocation,
                                    }}
                                    title="Your Location"
                                />
                                : console.log('null')
                        } */}
                        {
                            this.state.shopList.map((item) => {
                                return (
                                    <Marker
                                        coordinate={{
                                            latitude: parseFloat(item.location.latitude),
                                            longitude: parseFloat(item.location.longitude),
                                        }}
                                        title={user.language == 'th' ? item.lang.th.name : user.language == 'en' ? item.lang.en.name : ''}
                                        description={user.language == "th" ? "อ่านเพิ่มเติม..." : user.language == 'en' ? "Read More..." : ''}
                                        image={shopIcon}
                                        onCalloutPress={() => this.onClickMarkerShop(item)}
                                    />
                                )
                            })
                        }
                    </MapView>
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity style={styles.infoButton} onPress={() => this.onClickInfoButton()}>
                            <Icon name="info-circle" size={24} color="gray" />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.currentlocationButton} onPress={() => this.renderCurrentLocation()}>
                            <IconMat name="crosshairs-gps" size={24} color="gray" />
                        </TouchableOpacity>
                    </View>
                    {
                        this.state.visibleMarkShop == true ?
                            <View style={{ width: '100%' }}>
                                <Card full>
                                    <Card.Header
                                        title={user.language == 'th' ? <View style={{ width: 250 }}><ShopNameText numberOfLines={1}>{this.state.shop.lang.th.name}</ShopNameText></View> : user.language == 'en' ? <ShopNameText>{this.state.shop.lang.en.name}</ShopNameText> : ''}
                                        // thumbStyle={{ width: 30, height: 30 }}
                                        thumb={
                                            this.state.shop.category == 'ร้านค้า' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/shop.png')} /> :
                                                this.state.shop.category == 'ร้านอาหาร (และเครื่องดื่ม)' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/food-drink.png')} /> :
                                                    this.state.shop.category == 'เครื่องประดับ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/accessories.png')} /> :
                                                        this.state.shop.category == 'คาเฟ่และของหวาน' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/cafe.png')} /> :
                                                            this.state.shop.category == 'บริการต่างๆ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/service.png')} /> :
                                                                this.state.shop.category == 'อาหารไทย' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/thaiFood.png')} /> :
                                                                    this.state.shop.category == 'อาหารเอเชีย' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/noodles.png')} /> :
                                                                        this.state.shop.category == 'สินค้าเบ็ดเตล็ด และอื่นๆ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/more-item.png')} /> :
                                                                            this.state.shop.category == 'เฟอร์นิเจอร์และของตกแต่งบ้าน' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/forniture.png')} /> :
                                                                                this.state.shop.category == 'ของที่ระลึก' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/gift.png')} /> :
                                                                                    this.state.shop.category == 'ศูนย์การค้า' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/mall.png')} /> :
                                                                                        this.state.shop.category == 'ต้นไม้และอุปกรณ์ทำสวน' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/tree-icon.png')} /> :
                                                                                            this.state.shop.category == 'ของเก่า ของสะสม' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/collections.png')} /> :
                                                                                                this.state.shop.category == 'สุขภาพและความงาม' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/heart.png')} /> :
                                                                                                    this.state.shop.category == 'ตลาด' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/market-icon.png')} /> :
                                                                                                        this.state.shop.category == 'แฟชั่นสตรี' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/fashion-women.png')} /> :
                                                                                                            this.state.shop.category == 'แฟชั่นบุรุษ' ? <Image style={{ resizeMode: 'contain', width: 20, height: 20, marginRight: 10 }} source={require('../assets/image/category-shop-color/fashion-men.png')} /> : <Icon style={{ right: 8 }} name="file-unknown" size={20} />
                                        }
                                        extra={
                                            <View style={{ alignItems: 'flex-end' }}>
                                                <TouchableOpacity style={{ paddingHorizontal: 10 }}>
                                                    <IconMat name="crosshairs-gps" size={24} />
                                                </TouchableOpacity>
                                            </View>}
                                    />
                                    <TouchableOpacity onPress={() => { this.navigationToShopPage(this.state.shop.id) }}>
                                        <Card.Body>
                                            <ShopItemContent>
                                                <Image style={{ width: 140, height: 160, resizeMode: 'contain' }} source={{ uri: this.state.shop.image }} />
                                                <View style={styles.width}>
                                                    <ShopItemContent>
                                                        <View style={styles.border}>
                                                            <TouchableOpacity style={styles.menuButton} >
                                                                <IconFont style={{ marginStart: 5 }} name="star" size={18} color='#ffb31a' />
                                                                <View style={{ margin: 5 }}><Text>{this.state.shop.averageRate}</Text></View>
                                                            </TouchableOpacity>
                                                        </View>
                                                        <View style={styles.border}>
                                                            <TouchableOpacity style={styles.menuButton} >
                                                                <IconFont5 style={{ marginStart: 5 }} name="map-marker-alt" size={18} color='#999999' />
                                                                <View style={{ margin: 5 }}><Text>5 km.</Text></View>
                                                            </TouchableOpacity>
                                                        </View>
                                                        {this.state.shop.highlighted == true ? <IconMat style={{ margin: 4 }} name="trophy-award" size={20} color="#9900cc" /> : console.log()}
                                                    </ShopItemContent>
                                                    <View style={{ width: '60%' }}>
                                                        <Text numberOfLines={5}>
                                                            {user.language == 'th' ? this.state.shop.lang.th.description : user.language == 'en' ? this.state.shop.lang.en.description : ''}
                                                        </Text>
                                                    </View>
                                                </View>
                                            </ShopItemContent>
                                        </Card.Body>
                                        {/* <Card.Footer content="footer content" extra="footer extra content" /> */}
                                    </TouchableOpacity>
                                </Card>
                            </View>
                            : console.log()
                    }
                </View>
            );
        } else {
            return (
                <View>

                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        height: '92%',
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    buttonContainer: {
        flexDirection: 'column',
        marginVertical: 20,
        backgroundColor: 'transparent',
    },
    currentlocationButton: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        padding: 15,
        marginTop: 15,
        marginRight: 15,
        borderRadius: 50,
        elevation: 6,
        backgroundColor: 'white'
    },
    infoButton: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        padding: 15,
        marginRight: 15,
        marginBottom: 5,
        borderRadius: 50,
        elevation: 6,
        backgroundColor: 'white'
    },
    menuButton: {
        borderRadius: 8,
        margin: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    border: {
        backgroundColor: '#cccccc',
        borderRadius: 8,
        margin: 2
    },
    width: {
        width: '100%'
    },
    height: {
        height: '100%'
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    { push }
)(MapPage)