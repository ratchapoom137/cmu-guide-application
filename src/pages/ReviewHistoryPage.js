import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, Image, ScrollView, Alert, TouchableOpacity } from 'react-native';
import { Icon, Card, SwipeAction } from '@ant-design/react-native';
import { connect } from 'react-redux'
import { push, goBack } from 'connected-react-router'
import axios from 'axios';
import _ from 'lodash'
import IconFont from 'react-native-vector-icons/FontAwesome';
import IconFont5 from 'react-native-vector-icons/FontAwesome5';
import {
    ROUTE_SHOP,
    ROUTE_WRITEREVIEW,
    ROUTE_LOADER
} from '../constants/RouteConstants'
import {
    BodyMain,
    FlexOne
} from '../components/General.styled'
import SubHeader from '../components/general/SubHeader'
import Loader from '../components/items/Loader'
var options = { weekday: 'short', month: 'short', day: 'numeric' }

class ReviewHistoryPage extends Component {

    state = {
        reviewList: [],
        shopList: [],
        reviewShop: [],
        buttonOption: [
            {
                text: 'Edit',
                onPress: () => this.onPressEditButton(),
                style: { backgroundColor: 'orange', color: 'white' },
            },
            {
                text: 'Delete',
                onPress: () => this.onPressDeleteButton(),
                style: { backgroundColor: 'red', color: 'white' },
            },
        ],
        itemOnFocus: [],
        loading: true
    }

    componentWillMount() {
        this.getReviewByCustomerId()
    }

    getReviewByCustomerId = () => {
        const { user } = this.props
        axios({
            url: `http://35.173.241.14:8888/review/user/${user.information.user.id}`,
            method: 'get',
            headers: { "Authorization": `Bearer ${user.information.token}` }

        }).then(response => {
            this.setState({
                reviewList: response.data,
                loading: false
            })
            // this.renderShopById(response.data.shopId)
            this.state.reviewList.map((item, index) => {
                console.log("reviewList jaaaa", this.state.reviewList);
                console.log("itemmssssss: ", item.shopId);
                this.renderShopById(item.shopId)
            })

        }).catch(e => {
            alert('not successed' + e.response.data)
            console.log('res data: ', e.response.data);
            console.log('res data: ', e.response);
        })
    }

    renderShopById = (id) => {
        console.log("kutyyy", id);

        axios.get(`https://chiangmai.thaimarket.guide/shop/${id}`)
            .then(response => {
                this.setState({
                    shopList: [...this.state.shopList, response.data.data]
                })
                console.log("PPPPPPPPPPP", this.state.shopList);
                console.log("idddddddddd", id);
                // this.state.shopList.push(response.data)
                this.addItem()
            })
            .catch((error) => {
                console.error('Error: ' + error);
            })
    }

    addItem = () => {
        console.log("KKKKKKKKKKKKERRRRRRR", this.state.reviewList);
        console.log("wwwwwwwwwwwww", this.state.shopList);
        const addShopInReview = _.map(this.state.reviewList, (item) => {
            const map = _.filter(this.state.shopList, { 'id': item.shopId })
            return {
                ...item,
                shop: map
            }
        })
        // const test = _.map(this.state.shopList, (item) => {
        //     const map = _.filter(this.state.reviewList, { 'shopId': item.id })
        //     return {
        //         ...item,
        //         review: map
        //     }
        // })
        this.setState({
            reviewShop: addShopInReview
        })
    }

    renderStar = (item) => {
        let star = [
            { color: '#999999', iconName: 'star-o' },
            { color: '#999999', iconName: 'star-o' },
            { color: '#999999', iconName: 'star-o' },
            { color: '#999999', iconName: 'star-o' },
            { color: '#999999', iconName: 'star-o' }
        ]

        for (let i = 0; i < Math.round(item); i++) {
            star[i].color = '#ffb31a';
            star[i].iconName = 'star';
        }
        return (
            <View style={styles.boxStar}>
                <IconFont name={star[0].iconName} size={16} color={star[0].color} />
                <IconFont name={star[1].iconName} size={16} color={star[1].color} />
                <IconFont name={star[2].iconName} size={16} color={star[2].color} />
                <IconFont name={star[3].iconName} size={16} color={star[3].color} />
                <IconFont name={star[4].iconName} size={16} color={star[4].color} />
            </View>
        )
    }

    renderDate = (timestamp) => {
        var date = new Date(timestamp).toLocaleDateString("th-TH", options)
        var time = new Date(timestamp).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
        return (
            <Text>{date} at {time}</Text>
        )
    }

    onPressEditButton = () => {
        console.log("item ssssssssss", this.state.itemOnFocus);
        this.props.push(ROUTE_WRITEREVIEW, { item: this.state.itemOnFocus })
    }

    onPressDeleteButton = () => {
        const { user } = this.props
        Alert.alert(
            'ต้องการที่จะลบรีวิว ?',
            '',
            [{
                text: 'ลบรีวิว', onPress: () => {
                    axios({
                        url: `http://35.173.241.14:8888/review/delete/${this.state.itemOnFocus.id}`,
                        method: 'delete',
                        headers: { "Authorization": `Bearer ${user.information.token}` }
                    }).then(res => {
                        alert('Delete successed')
                        this.props.push(ROUTE_LOADER)
                    }).catch(e => {
                        alert('Delete not successed' + e.response.data)
                        console.log('res data: ', e.response.data);
                        console.log('res data: ', e.response);
                    })
                }
            }]
        );
    }

    navigationToShopPage = (id) => {
        const { push } = this.props
        push(ROUTE_SHOP, { id })
    }

    _keyExtractor = (item, index) => item.id;

    render() {
        const { user, data } = this.props
        console.log("State Review in render: ", this.state.reviewList);
        console.log("State Shop in render: ", this.state.shopList);
        console.log("State reviewShop in render: ", this.state.reviewShop);
        // if (this.state.reviewList.length > 0 && this.state.reviewShop.length > 0) {
        return (
            <BodyMain>
                <SubHeader title={user.language == 'th' ? 'ประวัติการรีวิว' : user.language == 'en' ? 'Review History' : ''} hideGoBack={'back'} />
                <ScrollView>
                    <FlatList
                        numColumns={1}
                        data={this.state.reviewShop}
                        keyExtractor={this._keyExtractor}
                        renderItem={(item) => item.item.shop.length > 0 && item.item.deleted === false ? (
                            <SwipeAction
                                autoClose
                                style={{ backgroundColor: 'transparent' }}
                                right={this.state.buttonOption}
                                onOpen={() => this.setState({ itemOnFocus: item.item })}
                                onClose={() => this.setState({ itemOnFocus: item.item })}
                            >
                                <TouchableOpacity onPress={() => { this.navigationToShopPage(item.item.shop[0].id) }}>
                                    <Card full>
                                        <Card.Body>
                                            <View style={{ flexDirection: 'row', margin: 10, marginLeft: 15 }}>
                                                <Image
                                                    style={styles.imageProfileUser}
                                                    source={{ uri: item.item.shop[0].image }}
                                                />
                                                <View style={{ flex: 1 }}>
                                                    <Text style={{ fontSize: 18, color: 'black', fontWeight: '500' }}>{item.item.shop[0].lang.th.name}</Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Text style={{ color: '#ffb31a' }}>{item.item.rate}.0</Text>
                                                        {this.renderStar(item.item.rate)}
                                                    </View>
                                                    {this.renderDate(item.item.date)}
                                                </View>
                                            </View>
                                            <View style={{ marginLeft: 25, width: '90%' }}>
                                                <Text>{item.item.comment}</Text>
                                            </View>
                                        </Card.Body>
                                        {/* <Card.Footer content="footer content" extra="footer extra content" /> */}
                                    </Card>
                                </TouchableOpacity>
                            </SwipeAction>
                        ) : (null)}
                    />
                </ScrollView>
                <Loader
                    modalVisible={this.state.loading}
                    animationType="fade"
                />
            </BodyMain>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    imageProfileUser: {
        width: 70,
        height: 70,
        resizeMode: 'cover',
        borderRadius: 70 / 2,
        marginRight: 15
    },
    boxStar: {
        marginLeft: 5,
        flexDirection: 'row'
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    { push, goBack }
)(ReviewHistoryPage)
