import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, Alert, TouchableOpacity } from 'react-native';
import { Provider, Button, Modal, Grid, Icon } from '@ant-design/react-native';
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import axios from 'axios';
import SubHeader from '../components/general/SubHeader'
import {
    Content,
    BodyMain,
    Body,
    Center,
    ShopNameText,
    ShopItemContent
} from '../components/General.styled'
import {
    ROUTE_LOGIN,
    ROUTE_TRIPDETAIL,
    ROUTE_ADDTRIP
} from '../constants/RouteConstants'

class TripPage extends Component {

    state = {
        modal: false,
        tripList: []
    }

    componentDidMount() {
        this.renderTrip()
    }

    renderTrip = () => {
        const { user } = this.props
        // console.log('user suss', user.information);

        axios({
            url: `http://35.173.241.14:8888/trip/list/${user.information.user.id}`,
            method: 'get',
            headers: { "Authorization": `Bearer ${user.information.token}` },
        }).then(res => {
            this.setState({
                tripList: res.data
            })
            console.log("sdasdaresposne", res.data);

        }).catch(e => {
            alert('not successed' + e.response.data)
            console.log('res data: ', e.response.data);
            console.log('res data: ', e.response);
        })
    }

    onPressAddTrip = () => {
        const { push } = this.props
        push(ROUTE_ADDTRIP)

    }

    onPressTripItem = (id) => {
        const { push } = this.props
        console.log("Check id", id);

        push(ROUTE_TRIPDETAIL, { id })
    }

    onPressDeleteButton = (tripId) => {
        const { data, user } = this.props
        Alert.alert(
            'ต้องการที่จะลบ ?',
            '',
            [{
                text: 'ลบ', onPress: () => {
                    axios({
                        url: `http://35.173.241.14:8888/trip/delete/${tripId}`,
                        method: 'delete',
                        headers: { "Authorization": `Bearer ${user.information.token}` }
                    }).then(res => {
                        alert('Delete successed')
                        this.renderTrip()
                    }).catch(e => {
                        alert('Delete not successed' + e.response.data)
                        console.log('res data: ', e.response.data);
                        console.log('res data: ', e.response);
                    })
                }
            }]
        );
    }

    render() {
        const { user } = this.props
        // const footerButtons = [
        //     { text: 'Cancel', onPress: () => this.setState({ modal: false }) },
        //     { text: 'Ok', onPress: () => this.onPressAddTrip() },
        // ];
        return (
            <BodyMain>
                <SubHeader title={user.language == 'th' ? 'ทริปของฉัน' : user.language == 'en' ? 'My Trip' : ''} hideGoBack={'back'} />
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                        <Button
                            style={styles.shadow}
                            size="small"
                            onPress={() => this.onPressAddTrip()}
                        >
                            {user.language == 'th' ? 'เพิ่มทริป' : user.language == 'en' ? 'Add Trip' : ''}
                        </Button>
                    </View>
                    <View style={{ marginLeft: 20, marginBottom: 10 }}>
                        <Text style={{ fontSize: 20 }}>{user.language == 'th' ? 'จำนวน: ' : user.language == 'en' ? 'Number: ' : ''}{this.state.tripList.length}</Text>
                    </View>

                    <Grid
                        data={this.state.tripList}
                        columnNum={2}
                        onPress={(item, index) => alert(index)}
                        renderItem={(item, index) => (
                            <TouchableOpacity
                                style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}
                                onPress={() => this.onPressTripItem(item.id)}
                            >
                                <View style={{ width: '100%', alignItems: 'flex-end' }}>
                                    <TouchableOpacity onPress={() => this.onPressDeleteButton(item.id)}>
                                        <Icon name="delete" size={25} color="#e60000" />
                                    </TouchableOpacity>
                                </View>

                                <Image source={require("../assets/image/myTrip.png")}
                                    style={styles.imageProducts}
                                />
                                <Center >
                                    <Text numberOfLines={1}>{user.language == 'th' ? 'ชื่อทริป: ' : user.language == 'en' ? 'Trip Name: ' : ''} {item.name}</Text>
                                    <Text numberOfLines={1}>{user.language == 'th' ? 'รายละเอียด' : user.language == 'en' ? 'Description' : ''} {item.description}</Text>
                                </Center>
                            </TouchableOpacity>
                        )}
                    />
                </ScrollView>

            </BodyMain>
        )
    }
}

const styles = StyleSheet.create({
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 6,
        borderRadius: 40,
        width: 100,
        height: 40,
        margin: 15
    },
    boxImageProduct: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 6,
        height: '70%',
        backgroundColor: 'white'
    },
    imageProducts: {
        width: 85,
        height: 115,
        resizeMode: 'contain',
        marginRight: 5,
        marginLeft: 5
    },
})

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    { push }
)(TripPage)