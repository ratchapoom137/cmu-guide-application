import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity, ImageBackground, Animated, Button } from 'react-native';
import { Grid, Icon, Carousel } from '@ant-design/react-native'
import { goBack } from 'connected-react-router'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import Header from '../components/general/Header'
import Loader from '../components/items/Loader'
import {
    BodyMain,
    Content,
    ImageCarousel,
    TextCarousel,
    BoxTextCarousel,
    CarouselBox,
    Menu,
    ItemMenu,
    MenuStyle,
} from '../components/General.styled'
import {
    ROUTE_SHOPLIST, ROUTE_SHOP
} from '../constants/RouteConstants'

const data = Array.from(new Array(16)).map((_val, i) => ({
    icon: 'https://os.alipayobjects.com/rmsportal/IptWdCkrtkAUfjE.png',
    text: `Name${i}`,
}));

class ImageLoader extends Component {
    state = {
        opacity: new Animated.Value(0),
    }

    onLoad = () => {
        Animated.timing(this.state.opacity, {
            toValue: 1,
            duration: 500,
            useNativeDriver: true,
        }).start();
    }

    render() {
        return (
            <Animated.Image
                onLoad={this.onLoad}
                {...this.props}
                style={[
                    {
                        opacity: this.state.opacity,
                        transform: [
                            {
                                scale: this.state.opacity.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [0.85, 1],
                                })
                            }
                        ]
                    }, this.props.style,
                ]}
            />
        )
    }
}

class HomePage extends Component {

    state = {
        // category: [
        //     { image: 'shop.png', name: 'ร้านค้า' }, { image: '../assets/image/category-shop-color/shop.png', name: 'ร้านอาหาร (และเครื่องดื่ม)' }, { image: '../assets/image/category-shop-color/shop.png', name: 'เครื่องประดับ' }, { image: '../assets/image/category-shop-color/shop.png', name: 'คาเฟ่และของหวาน' }, { image: '../assets/image/category-shop-color/shop.png', name: 'บริการต่างๆ' }, { image: '../assets/image/category-shop-color/shop.png', name: 'อาหารไทย' }, { image: '../assets/image/category-shop-color/shop.png', name: 'อาหารเอเชีย' }, { image: '../assets/image/category-shop-color/shop.png', name: 'สินค้าเบ็ดเตล็ด และอื่นๆ' },
        //     { image: '../assets/image/category-shop-color/shop.png', name: 'เฟอร์นิเจอร์และของตกแต่งบ้าน' }, { image: '../assets/image/category-shop-color/shop.png', name: 'ของที่ระลึก' }, { image: '../assets/image/category-shop-color/shop.png', name: 'ศูนย์การค้า' }, { image: '../assets/image/category-shop-color/shop.png', name: 'ต้นไม้และอุปกรณ์ทำสวน' }, { image: '../assets/image/category-shop-color/shop.png', name: 'ของเก่า ของสะสม' }, { image: '../assets/image/category-shop-color/shop.png', name: 'สุชภาพและความงาม' }, { image: '../assets/image/category-shop-color/shop.png', name: 'ตลาด' }, { image: '../assets/image/category-shop-color/shop.png', name: 'แฟชั่นสตรี' },
        //     { image: '../assets/image/category-shop-color/shop.png', name: 'แฟชั่นบุรุษ' }
        // ],
        cate: [
            { name: 'ร้านค้า' }, { name: 'ร้านอาหาร (และเครื่องดื่ม)' }, { name: 'เครื่องประดับ' }, { name: 'คาเฟ่และของหวาน' }, { name: 'บริการต่างๆ' }, { name: 'อาหารไทย' }, { name: 'อาหารเอเชีย' }, { name: 'สินค้าเบ็ดเตล็ด และอื่นๆ' }, { name: 'เฟอร์นิเจอร์และของตกแต่งบ้าน' },
            { name: 'ของที่ระลึก' }, { name: 'ศูนย์การค้า' }, { name: 'ต้นไม้และอุปกรณ์ทำสวน' }, { name: 'ของเก่า ของสะสม' }, { name: 'สุขภาพและความงาม' }, { name: 'ตลาด' }, { name: 'แฟชั่นสตรี' }, { name: 'แฟชั่นบุรุษ' },
            {}, {}, {}, {}, {}, {}, {},
        ]
    }

    navigateToListShopPage = () => {
        const { push } = this.props
        push(ROUTE_SHOPLIST, { category: 'รวม' })
    }

    onPressCategoryItem = (item) => {
        const { push } = this.props
        push(ROUTE_SHOPLIST, { category: item.name })
    }

    render() {
        const { user } = this.props
        return (
            <BodyMain>
                <Header title='CMU GUIDE' />
                <CarouselBox>
                    <Carousel
                        selectedIndex={0}
                        autoplay
                        infinite
                        dots={false}
                    >
                        <ImageCarousel
                            source={require('../assets/image/imageSlide-1.jpg')} >
                            <BoxTextCarousel>
                                <TextCarousel>
                                    {user.language == 'th' ? 'กาดหน้ามอ...มีของขายเยอะ!' : user.language == 'en' ? 'Kad Na Mor' : ''}
                                </TextCarousel>
                            </BoxTextCarousel>
                        </ImageCarousel>
                        <ImageCarousel
                            source={require('../assets/image/imageSlide-2.jpg')} >
                            <BoxTextCarousel>
                                <TextCarousel>
                                    {user.language == 'th' ? 'กาดหลังมอ...ของกินเพียบ' : user.language == 'en' ? 'Kad Lang Mor' : ''}
                                </TextCarousel>
                            </BoxTextCarousel>
                        </ImageCarousel>
                        <ImageCarousel
                            source={require('../assets/image/imageSlide-3.jpg')} >
                            <BoxTextCarousel>
                                <TextCarousel>
                                    {user.language == 'th' ? 'สถานที่ต้องไป "บ้านข้างวัด"' : user.language == 'en' ? 'Baan Kang Wat' : ''}
                                </TextCarousel>
                            </BoxTextCarousel>
                        </ImageCarousel>
                        <ImageCarousel
                            source={require('../assets/image/imageSlide-4.jpg')} >
                            <BoxTextCarousel>
                                <TextCarousel>
                                    {user.language == 'th' ? 'Shopping : ที่ใกล้ที่สุด' : user.language == 'en' ? 'Maya' : ''}
                                </TextCarousel>
                            </BoxTextCarousel>
                        </ImageCarousel>
                        <ImageCarousel
                            source={require('../assets/image/imageSlide-5.jpg')} >
                            <BoxTextCarousel>
                                <TextCarousel>
                                    {user.language == 'th' ? 'ร้านกาแฟ ที่นิยมหลังมอ' : user.language == 'en' ? 'Roastniyom Coffee' : ''}
                                </TextCarousel>
                            </BoxTextCarousel>
                        </ImageCarousel>
                    </Carousel>
                </CarouselBox>
                <Content>
                    <View style={{ width: '100%' }}>
                        <Grid
                            data={this.state.cate}
                            columnNum={4}
                            isCarousel
                            onPress={(item, index) => {this.onPressCategoryItem(item)}}
                            itemStyle={{ height: 60, width: 80, justifyContent: 'center', alignItems: 'center' }}
                            renderItem={(item, index) => (
                                <View style={{ alignItems: 'center', justifyContent: 'center', width: '90%' }}>
                                    {
                                        item.name == 'ร้านค้า' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/shop.png')} /> :
                                            item.name == 'ร้านอาหาร (และเครื่องดื่ม)' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/food-drink.png')} /> :
                                                item.name == 'เครื่องประดับ' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/accessories.png')} /> :
                                                    item.name == 'คาเฟ่และของหวาน' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/cafe.png')} /> :
                                                        item.name == 'บริการต่างๆ' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/service.png')} /> :
                                                            item.name == 'อาหารไทย' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/thaiFood.png')} /> :
                                                                item.name == 'อาหารเอเชีย' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/noodles.png')} /> :
                                                                    item.name == 'สินค้าเบ็ดเตล็ด และอื่นๆ' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/more-item.png')} /> :
                                                                        item.name == 'เฟอร์นิเจอร์และของตกแต่งบ้าน' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/forniture.png')} /> :
                                                                            item.name == 'ของที่ระลึก' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/gift.png')} /> :
                                                                                item.name == 'ศูนย์การค้า' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/mall.png')} /> :
                                                                                    item.name == 'ต้นไม้และอุปกรณ์ทำสวน' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/tree-icon.png')} /> :
                                                                                        item.name == 'ของเก่า ของสะสม' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/collections.png')} /> :
                                                                                            item.name == 'สุขภาพและความงาม' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/heart.png')} /> :
                                                                                                item.name == 'ตลาด' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/market-icon.png')} /> :
                                                                                                    item.name == 'แฟชั่นสตรี' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/fashion-women.png')} /> :
                                                                                                        item.name == 'แฟชั่นบุรุษ' ? <Image style={{ resizeMode: 'contain', width: 30, height: 30 }} source={require('../assets/image/category-shop-color/fashion-men.png')} /> : null

                                    }
                                    <Text style={{ fontSize: 10 }} numberOfLines={1}>{item.name}</Text>
                                </View>
                            )}
                        />
                    </View>

                    <Menu>
                        <MenuStyle>
                            <ItemMenu
                                onPress={() => this.navigateToListShopPage()}
                                style={styles.shadow}
                            >
                                <ImageLoader source={require('../assets/image/shop-icon5.png')}
                                    style={{ width: 50, height: 50, resizeMode: 'contain' }} />

                            </ItemMenu>
                            <Text>{user.language == 'th' ? 'ร้านค้า' : user.language == 'en' ? 'Shop' : ''}</Text>
                        </MenuStyle>
                        <MenuStyle>
                            <ItemMenu category
                                onPress={() => this.navigateToListShopPage()}
                                style={styles.shadow}
                            >
                                <ImageLoader source={require('../assets/image/categoryShop.jpg')}
                                    style={{ width: 70, height: 70, resizeMode: 'contain' }} />
                            </ItemMenu>
                            <Text style={{ fontSize: 12 }}>{user.language == 'th' ? 'หมวดหมู่ร้านค้า' : user.language == 'en' ? 'Shop Category' : ''}</Text>
                        </MenuStyle>
                    </Menu>
                </Content>
            </BodyMain>
        )
    }
}
const styles = StyleSheet.create({
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 6,
        backgroundColor: 'white'
    }
})


const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    { push }
)(HomePage)