import styled from 'styled-components'

const { props } = this

export const Body = styled.View`
    flex: 1;
    align-items: center;
    justify-content: center;
    background-color: ${props => (props.menu ? "#f1f1f1" : "#976bae")};
`

export const BodyMain = styled.View`
    flex: 1;
    background-color: #ffffff
`

export const Flex = styled.View`
    flex: 1;
    width: ${props => props.backButton ? '100%' : 'auto'};
    align-items: ${props => props.backButton ? 'flex-start' : 'center'};
    margin: ${props => props.backButton ? '20px' : '0px'};
`

export const FlexOne = styled.View`
    flex: 1;
`

export const Row = styled.View`
    flex-direction: row;
    align-items: ${props => props.products ? 'center' : 'flex-start'};
    justify-content: ${props => props.products ? 'center' : 'flex-start'};
`

export const TextCustomer = styled.Text`
    font-size: ${props => props.titleModal ? '20' : props.desModal ? '16' : props.infoModal ? '18' : '16'};
    color: ${props => props.moreButton ? '#7d5194' : props.titleModal ? 'white' : props.desModal ? '#a6a6a6': props.infoModal ? 'gray' : 'black'};
`

export const TouchableOpacityBackButton = styled.TouchableOpacity`
    background-color: ${props => props.full ? 'rgba(0,0,0,0.6)' : 'rgba(255,255,255,0.0)'};
    margin-left: 10px;
    border-radius: 50;
`

export const Content = styled.View`
    flex: 1;
    align-items: center;
    background-color: #ffffff;
`

export const Center = styled.View`
    align-items: center;
    justify-content: center;
`

export const MenuStyle = styled.View`
    align-items: center;
    justify-content: center;
    width: 82;
    margin: 20px;
`

export const SignupTextCont = styled.View`
    flex-grow: 1;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`
export const SignupText = styled.Text`
    color: rgba(255,255,255,0.6);
    font-size: 16
`

export const SignupButton = styled.Text`
    color: #ffffff;
    font-size: 16;
    font-weight: 500;
`

export const LoginButton = styled.View`
    width: 300;
    margin-vertical: 10;
    padding-horizontal: 12;
`

export const TextTabBar = styled.Text`
    font-size: 12;
    color: #cccccc;
`

export const TextTabBarSelected = styled.Text`
    font-size: 12;
    color: #ffbf80;
`

export const HeaderTag = styled.View`
    align-items: center;
    justify-content: center;
    background-color: #d9d9d9;
    width: 360px;
    height: 4%;
    flex-direction: row;
`

export const SubHeaderTag = styled.View`
    background-color: #7d5194;
    width: 360px;
    height: 8%;
    flex-direction: row;
`

export const BoxTextCarousel = styled.View`
    background-color: rgba(255,255,255,0.6);
    position: absolute;
    top: 130; left: 0;
    right: 0; bottom: 0;
    justify-content: flex-end;
    align-items: flex-start;
`

export const TextCarousel = styled.Text`
    color: #262626;
    font-size: 14;
`

export const ImageCarousel = styled.ImageBackground`
    align-items: center;
    justify-content: center;
    height: 150;
    width: 100%;
    resize-mode = contain;
`

export const CarouselBox = styled.View`
    height: 150;
`

export const Menu = styled.View`
    height: ${props => (props.sortBy ? "10%" : "34%")}
    width: 100%;
    align-items: center;
    justify-content: center;
    background-color: #f1f1f1;
    flex-direction: row;
`

export const ItemMenu = styled.TouchableOpacity`
    border-radius: 50;
    align-items: center;
    justify-content: center;
    background-color: #ffffff;
    padding-top: ${props => props.category ? '10px' : '20px'};
    padding-right: ${props => props.category ? '12px' : '22px'};
    padding-bottom: ${props => props.category ? '10px' : '20px'};
    padding-left: ${props => props.category ? '12px' : '22px'};
    margin-bottom: 2px;
`

export const SubHeaderText = styled.Text`
    color: #ffffff;
    font-size: 18;
`

export const GoBackBox = styled.TouchableOpacity`
    flex: ${props => props.titleHeader ? '2': '1'};
    justify-content: center;
    align-items: center;
`

export const ShopNameText = styled.Text`
    color: black;
    font-size: 20;
    font-weight: 500;
`

export const ShopItemContent = styled.View`
    flex-direction: row;
`

export const TextTitleInBoxShop = styled.Text`
    color: ${props => props.detail ? "#999999" : props.review ? "#ffb31a" : props.status ? 'white' : "white"};
    font-size: ${props => props.detail ? "20" : props.review ? "14" : props.status ? '14' : '26'};
    
`

export const BoxText = styled.View`
    padding-bottom: ${props => props.category ? "10" : "15"};
    flex-direction: row;
`

export const BoxBackgroudColor = styled.View`
    background-color: ${props => props.white ? 'white' : 'white'};
`

export const TouchGps1 = styled.TouchableOpacity`
    flex: 1;
    align-items: flex-end;
    justify-content: center
`

export const TouchGps2 = styled.TouchableOpacity`
    
`