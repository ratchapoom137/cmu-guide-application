import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { goBack } from 'connected-react-router'
import Footer from './Footer'
import {
    Body,
    Content
} from '../General.styled'

export default class Container extends Component {

    render() {
        const { children, tabName } = this.props
        return(
            <Body>
                <Content>{children}</Content>
                <Footer tabName={tabName} />
            </Body>
        )
    }

}

