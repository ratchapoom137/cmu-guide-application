import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { TabBar, Icon } from '@ant-design/react-native'
import { goBack } from 'connected-react-router'
import {
    HeaderTag
} from '../General.styled'
import Logo from '../../assets/Logo'

export default class Header extends Component {

    render() {
        const { title } = this.props
        return (
            <HeaderTag>
                {/* <Image style={{ width: '6%', height: '94%', borderRadius: 70, marginRight: 5 }} source={require('../../assets/image/logo.png')} /> */}
                <Text>{title}</Text>
            </HeaderTag>
        )
    }

}