import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Dimensions, StatusBar, ScrollView } from 'react-native';
import { Icon, Tabs } from '@ant-design/react-native';
import { TabNavigator } from 'react-navigation';
import { push, goBack } from 'connected-react-router'
import { connect } from 'react-redux'
import DetailTab from '../../pages/Tabs/DetailTab';
import PromotionTab from '../../pages/Tabs/PromotionTab';
import MapTab from '../../pages/Tabs/MapTab';
import ReviewTab from '../../pages/Tabs/ReviewTab';

class TabOnShopPage extends Component {

    state = {
        tabsEN: [
            { title: 'Details' },
            { title: 'Promotion' },
            { title: 'Map' },
            { title: 'Reviews' }
        ],
        tabsTH: [
            { title: 'รายละเอียด' },
            { title: 'โปรโมชั่น' },
            { title: 'แผนที่' },
            { title: 'รีวิว' }
        ]
    }

    render() {
        const { user, data, pathName } = this.props
        return (
            <Tabs
                tabs={user.language == 'th' ? this.state.tabsTH : user.language == 'en' ? this.state.tabsEN : ''}
                tabBarActiveTextColor="black"
                tabBarInactiveTextColor="#cccccc"
            >
                <DetailTab data={data} pathName={pathName} />
                <PromotionTab data={data} />
                <MapTab data={data} />
                <ReviewTab data={data} />
            </Tabs>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(
    mapStateToProps,
    { push, goBack }
)(TabOnShopPage)