import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { TabBar, Icon } from '@ant-design/react-native'
import { connect } from 'react-redux'
import { goBack } from 'connected-react-router'
import {
    SubHeaderTag,
    SubHeaderText,
    GoBackBox
} from '../General.styled'
import Logo from '../../assets/Logo'

class Header extends Component {

    render() {
        const { title, hideGoBack, goBack } = this.props
        return (
            <SubHeaderTag>
                <GoBackBox back onPress={goBack} >
                    <Icon name="left-circle" color="#ffcc99" size="lg" />
                </GoBackBox>
                <GoBackBox titleHeader>
                    <SubHeaderText numberOfLines={1}>{title}</SubHeaderText>
                </GoBackBox>
                <GoBackBox>
                    
                </GoBackBox>
            </SubHeaderTag>
        )
    }

}

export default connect(
    null,
    { goBack }
)(Header)
