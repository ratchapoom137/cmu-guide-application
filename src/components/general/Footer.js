import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image } from 'react-native';
import { TypeOne, TypeTwo } from "react-native-animated-bottom-tabbar"; //<-----------------import
import { Icon } from '@ant-design/react-native'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import { setLanguage } from '../../reducers/action'
import {
    Center,
    TextTabBar,
    TextTabBarSelected,
    Content
} from '../General.styled'
import HomePage from '../../pages/HomePage'
import MapPage from '../../pages/MapPage'
import MorePage from '../../pages/MorePage'
import ProfilePage from '../../pages/ProfilePage'

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

const HOME_EN = 'Home';
const MAP_EN = 'Map';
const PROFILE_EN = 'Profile';
const MORE_EN = 'More';

const HOME_TH = 'หน้าหลัก';
const MAP_TH = 'แผนที่';
const PROFILE_TH = 'ผู้ใช้';
const MORE_TH = 'อื่นๆ';
const lang = 'th'
//array of icon views this array can be image or vector icon for tab  bar
// array size can be 1 to maximum 5 !

class Footer extends Component {

    state = {
        icon: [
            <Center>
                <Icon name="home" color="#ffbf80" />
                <TextTabBarSelected>
                    {
                        this.props.user.language != undefined ? this.props.user.language == 'th' ? HOME_TH : this.props.user.language == 'en' ? HOME_EN : '' : HOME_TH
                    }
                </TextTabBarSelected>
            </Center>,
            <Center>
                <Icon name="environment" />
                <TextTabBar>
                    {
                        this.props.user.language != undefined ? this.props.user.language == 'th' ? MAP_TH : this.props.user.language == 'en' ? MAP_EN : '' : MAP_TH
                    }
                </TextTabBar>
            </Center>,
            <Center>
                <Icon name="profile" />
                <TextTabBar>
                    {
                        this.props.user.language != undefined ? this.props.user.language == 'th' ? PROFILE_TH : this.props.user.language == 'en' ? PROFILE_EN : '' : PROFILE_TH
                    }
                </TextTabBar>
            </Center>,
            <Center>
                <Icon name="ellipsis" />
                <TextTabBar>
                    {
                       this.props.user.language != undefined ? this.props.user.language == 'th' ? MORE_TH : this.props.user.language == 'en' ? MORE_EN : '' : MORE_TH
                    }
                </TextTabBar>
            </Center>,
        ],
        index: '1',
    }

    componentDidMount() {
        const { user, setLanguage } = this.props
        if (!user.language) {
            setLanguage(lang)
        }
    }

    navigationPage = (index) => {
        const { user } = this.props
        if (index == 1) {
            const icon = [
                <Center>
                    <Icon name="home" color="#ffbf80" />
                    <TextTabBarSelected>{user.language == 'th' ? HOME_TH : user.language == 'en' ? HOME_EN : ''}</TextTabBarSelected>
                </Center>,
                <Center>
                    <Icon name="environment" />
                    <TextTabBar>{this.props.user.language == 'th' ? MAP_TH : this.props.user.language == 'en' ? MAP_EN : ''}</TextTabBar>
                </Center>,
                <Center>
                    <Icon name="profile" />
                    <TextTabBar>{this.props.user.language == 'th' ? PROFILE_TH : this.props.user.language == 'en' ? PROFILE_EN : ''}</TextTabBar>
                </Center>,
                <Center>
                    <Icon name="ellipsis" />
                    <TextTabBar>{this.props.user.language == 'th' ? MORE_TH : this.props.user.language == 'en' ? MORE_EN : ''}</TextTabBar>
                </Center>,
            ]
            this.setState({ icon: icon })
            this.setState({ index: index })
        } else if (index == 2) {
            const icon = [
                <Center>
                    <Icon name="home" />
                    <TextTabBar>{this.props.user.language == 'th' ? HOME_TH : this.props.user.language == 'en' ? HOME_EN : ''}</TextTabBar>
                </Center>,
                <Center>
                    <Icon name="environment" color="#ffbf80" />
                    <TextTabBarSelected>{this.props.user.language == 'th' ? MAP_TH : this.props.user.language == 'en' ? MAP_EN : ''}</TextTabBarSelected>
                </Center>,
                <Center>
                    <Icon name="profile" />
                    <TextTabBar>{this.props.user.language == 'th' ? PROFILE_TH : this.props.user.language == 'en' ? PROFILE_EN : ''}</TextTabBar>
                </Center>,
                <Center>
                    <Icon name="ellipsis" />
                    <TextTabBar>{this.props.user.language == 'th' ? MORE_TH : this.props.user.language == 'en' ? MORE_EN : ''}</TextTabBar>
                </Center>,
            ]
            this.setState({ icon: icon })
            this.setState({ index: index })
        } else if (index == 3) {
            const icon = [
                <Center>
                    <Icon name="home" />
                    <TextTabBar>{this.props.user.language == 'th' ? HOME_TH : this.props.user.language == 'en' ? HOME_EN : ''}</TextTabBar>
                </Center>,
                <Center>
                    <Icon name="environment" />
                    <TextTabBar>{this.props.user.language == 'th' ? MAP_TH : this.props.user.language == 'en' ? MAP_EN : ''}</TextTabBar>
                </Center>,
                <Center>
                    <Icon name="profile" color="#ffbf80" />
                    <TextTabBarSelected>{this.props.user.language == 'th' ? PROFILE_TH : this.props.user.language == 'en' ? PROFILE_EN : ''}</TextTabBarSelected>
                </Center>,
                <Center>
                    <Icon name="ellipsis" />
                    <TextTabBar>{this.props.user.language == 'th' ? MORE_TH : this.props.user.language == 'en' ? MORE_EN : ''}</TextTabBar>
                </Center>,
            ]
            this.setState({ icon: icon })
            this.setState({ index: index })
        } else if (index == 4) {
            const icon = [
                <Center>
                    <Icon name="home" />
                    <TextTabBar>{this.props.user.language == 'th' ? HOME_TH : this.props.user.language == 'en' ? HOME_EN : ''}</TextTabBar>
                </Center>,
                <Center>
                    <Icon name="environment" />
                    <TextTabBar>{this.props.user.language == 'th' ? MAP_TH : this.props.user.language == 'en' ? MAP_EN : ''}</TextTabBar>
                </Center>,
                <Center>
                    <Icon name="profile" />
                    <TextTabBar>{this.props.user.language == 'th' ? PROFILE_TH : this.props.user.language == 'en' ? PROFILE_EN : ''}</TextTabBar>
                </Center>,
                <Center>
                    <Icon name="ellipsis" color="#ffbf80" />
                    <TextTabBarSelected>{this.props.user.language == 'th' ? MORE_TH : this.props.user.language == 'en' ? MORE_EN : ''}</TextTabBarSelected>
                </Center>,
            ]
            this.setState({ icon: icon })
            this.setState({ index: index })
        }
    }

    render() {
        const { tabName } = this.props
        return (
            <View style={styles.container}>
                <Content>{this.state.index == 1 ? <HomePage /> : this.state.index == 2 ? <MapPage /> : this.state.index == 3 ? <ProfilePage /> : <MorePage />}</Content>
                <TypeTwo // you can use TypeOne Also With the same settings
                    icons={this.state.icon} //array of icon views this array can be image or vector icon
                    selectedColor={'#976bae'} //color of selected item in tab bar
                    backgroundColor={'#7d5194'} //background color of tab bar
                    onSelect={(index) => this.navigationPage(index)} //on select an item , index starts at 1 :-D
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, { push, setLanguage })(Footer)
